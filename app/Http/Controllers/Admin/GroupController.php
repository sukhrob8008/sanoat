<?php

namespace App\Http\Controllers\Admin;

use App\Domain\Admin\Courses\Models\Course;
use App\Domain\Admin\Groups\Actions\StoreGroupAction;
use App\Domain\Admin\Groups\Actions\UpdateGroupAction;
use App\Domain\Admin\Groups\DTO\StoreGroupDTO;
use App\Domain\Admin\Groups\DTO\UpdateGroupDTO;
use App\Domain\Admin\Groups\Models\Group;
use App\Domain\Admin\Groups\Repositories\GroupRepository;
use App\Domain\Admin\Groups\Requests\StoreGroupRequest;
use App\Domain\Admin\Groups\Requests\UpdateGroupRequest;
use App\Domain\Admin\Students\Actions\StoreStudentAction;
use App\Domain\Admin\Students\DTO\StoreStudentDTO;
use App\Domain\Admin\Students\Models\Student;
use App\Domain\Admin\Students\Requests\StoreStudentRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;

class GroupController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index(GroupRepository $repository)
    {
        $groups = $repository->getAll();
        return view('admin.groups.index', compact('groups'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        $courses = Course::all();
        return view('admin.groups.create', compact('courses'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function store(StoreGroupRequest $request, StoreGroupAction $action)
    {
        try {
            $dto = StoreGroupDTO::fromArray($request->all());
            $action->execute($dto);
        }catch (\Exception $exception)
        {
            return redirect()->back();
        }
        Alert::success('Success Title', 'Гуруҳ мувофаққиятли яратилди');
        return redirect()->route('group.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param Group $group
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|void
     */
    public function edit(Group $group)
    {
        $courses = Course::all();
        $start_date = date("Y-m-d", strtotime($group->start_date));
        $end_date = date("Y-m-d", strtotime($group->end_date));
        return view('admin.groups.edit', compact('group', 'courses','start_date','end_date'));
    }

    public function edit_org($course_id, $group_id)
    {
        $courses = Course::all();
        $course = DB::table('courses')
            ->where('id','=', $course_id)->first();
        $group = Group::where('id','=',$group_id)->first();
        $start_date = date("Y-m-d", strtotime($group->start_date));
        $end_date = date("Y-m-d", strtotime($group->end_date));
        return view('admin.groups.edit_org', compact('group', 'courses', 'course', 'start_date','end_date'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function update(UpdateGroupRequest $request, Group $group, UpdateGroupAction $action)
    {
        try {
            $request->merge([
                'group'=>$group
            ]);
            $dto = UpdateGroupDTO::fromArray($request->all());
            $action->execute($dto);
        }catch (\Exception $exception)
        {
            return redirect()->back();
        }
        Alert::success('Success Title', 'Гуруҳ мувофаққиятли ўзгартирилди');
        return redirect()->route('group.index');
    }

    /**
     * @param UpdateGroupRequest $request
     * @param Group $group
     * @param UpdateGroupAction $action
     * @return string
     */
    public function update_org(UpdateGroupRequest $request,$course_id, $group_id, UpdateGroupAction $action)
    {
        try {
            $group = Group::where('id','=',$group_id)->first();
//            $groups = Group::where('course_id','=',$course_id)->get();
//            $course = DB::table('courses')
//                ->where('id','=', $course_id)
//                ->first();

            $request->merge([
                'group'=>$group
            ]);
            $dto = UpdateGroupDTO::fromArray($request->all());
            $action->execute($dto);
        }catch (\Exception $exception)
        {
            return redirect()->back();
        }
        Alert::success('Success Title', 'Гуруҳ мувофаққиятли ўзгартирилди');
        return redirect()->route('course.itemGroup', $course_id);
//        return view('admin.groups.groups_show', compact('course','groups'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param Group $group
     * @return \Illuminate\Http\RedirectResponse
     */
    public function active(Group $group)
    {
        DB::table('groups')
            ->where('id','=', $group->id)
            ->update([
                'active'=>0
            ]);
        Alert::success('Success Title', 'Гуруҳ мувофаққиятли тугатилди');
        return redirect()->route('group.index');
    }

    /**
     * @param Group $group
     * @return string
     */
    public function active_org($course_id, $group_id)
    {
        DB::table('groups')
            ->where('id','=', $group_id)
            ->update([
                'active'=>0
            ]);
        $course = DB::table('courses')
            ->where('id','=', $course_id)
            ->orderBy('created_at','desc')
            ->first();
        $groups = Group::where('course_id','=',$course_id)
            ->orderBy('created_at','desc')
            ->get();
//        Alert::success('Success Title', 'Гуруҳ мувофаққиятли тугатилди');

        return view('admin.groups.groups_show', compact('course','groups'));
    }

    /**
     * @param Group $group
     * @return \Illuminate\Http\RedirectResponse
     */
    public function noactive(Group $group)
    {
        DB::table('groups')
            ->where('id','=', $group->id)
            ->update([
                'active'=>1
            ]);
        Alert::success('Success Title', 'Гуруҳ мувофаққиятли активлаштирилди');
        return redirect()->route('group.index');
    }

    public function noactive_org($course_id, $group_id)
    {
        DB::table('groups')
            ->where('id','=', $group_id)
            ->update([
                'active'=>1
            ]);
        $course = DB::table('courses')
            ->where('id','=', $course_id)
            ->orderBy('created_at','desc')
            ->first();
        $groups = Group::where('course_id','=',$course_id)
            ->orderBy('created_at','desc')
            ->get();
//        Alert::success('Success Title', 'Гуруҳ мувофаққиятли активлаштирилди');
        return view('admin.groups.groups_show', compact('course','groups'));
    }

    /**
     * @param Group $group
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|void
     */
    public function group_item(Group $group)
    {
        $students = Student::where('group_id','=',$group->id)
            ->orderBy('created_at','desc')
            ->get();
        return view('admin.students.students_show', compact('students', 'group'));
    }
}
