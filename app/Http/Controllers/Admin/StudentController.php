<?php

namespace App\Http\Controllers\Admin;

use App\Domain\Admin\Groups\Models\Group;
use App\Domain\Admin\Guvohnoma\Models\Guvohnoma;
use App\Domain\Admin\Students\Actions\StoreStudentAction;
use App\Domain\Admin\Students\Actions\UpdateStudentAction;
use App\Domain\Admin\Students\DTO\StoreStudentDTO;
use App\Domain\Admin\Students\DTO\UpdateStudentDTO;
use App\Domain\Admin\Students\Models\Student;
use App\Domain\Admin\Students\Requests\StoreStudentRequest;
use App\Domain\Admin\Students\Requests\UpdateStudentRequest;
use App\Http\Controllers\Controller;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use RealRashid\SweetAlert\Facades\Alert;
use Yajra\DataTables\Facades\DataTables;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        if($request->search == null){
            $students = Student::query()
                ->orderBy('id', 'desc')
                ->paginate(20);
        }elseif($request->search != null){
            $students = Student::query()
                ->orWhere('student_fio','like','%'.$request->search.'%')
                ->orWhere('number','=',$request->search)
                ->orWhere('pasport_seria','like','%'.$request->search.'%')
                ->orWhere('pasport_number','like','%'.$request->search.'%')
                ->whereHas('group_name', function ($query) use ($request) {
                    $query->where('group_title','=',$request->search);
                })
                ->whereHas('group_name.kurslar', function ($query) use ($request) {
                    $query->where('course_title','like','%'.$request->search.'%');
                })
                ->paginate(20);
        }


        return view('admin.students.index', compact('students'));
    }

    /**
     * @return \Illuminate\Database\Eloquent\Builder[]|\Illuminate\Database\Eloquent\Collection|JsonResponse
     */
    public function getDataToJSON()
    {
        $students = Student::query()
//            ->select('student_fio','group_id','number','pasport_seria','pasport_number','qr_name','guvohnoma_id')
            ->orderBy('id', 'desc')
            ->get();
        return response()
            ->json([
                'data' => $students
            ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\Http\Response
     */
    public function create()
    {
        $groups = Group::where('active', '=', 1)->get();
        $guvohnomas = Guvohnoma::all();
        return view('admin.students.create', compact('groups', 'guvohnomas'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function store(StoreStudentRequest $request, StoreStudentAction $action)
    {
        try {
            $dto = StoreStudentDTO::fromArray($request->all());
            $action->execute($dto);
        } catch (\Exception $exception) {
            return redirect()->back();
        }
        Alert::success('Success Title', 'Тингловчи мувофаққиятли яратилди');
        return redirect()->route('student.index');
    }

    /**
     * @param Student $student
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|void
     */
    public function show(Student $student)
    {
        return view('admin.students.show', compact('student'));
    }

    /**
     * @param Student $student
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|void
     */
    public function edit(Student $student)
    {
        $groups = Group::all();
        $guvohnomas = Guvohnoma::all();
        return view('admin.students.edit', compact('student', 'groups', 'guvohnomas'));
    }

    public function editGroupStudent(Student $student)
    {
        $group = Group::find($student->group_id);
        $groups = Group::all();
        $guvohnomas = Guvohnoma::all();
        return view('admin.groups.editGroupStudent', compact('student', 'groups', 'guvohnomas', 'group'));
    }

    public function updateGroupStudent(UpdateStudentRequest $request, Student $student, UpdateStudentAction $action)
    {
        try {
            $request->merge([
                'student' => $student
            ]);
            $dto = UpdateStudentDTO::fromArray($request->all());
            $action->execute($dto);
        } catch (\Exception $exception) {
            return redirect()->back();
        }
        Alert::success('Success Title', 'Тингловчи маълумотлари мувофаққиятли ўзгартирилди');
        return redirect()->route('group.itemStudent', $request->group_id);
    }

    /**
     * @param UpdateStudentRequest $request
     * @param Student $student
     * @param UpdateStudentAction $action
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|void
     */
    public function update(UpdateStudentRequest $request, Student $student, UpdateStudentAction $action)
    {
        try {
            $request->merge([
                'student' => $student
            ]);
            $dto = UpdateStudentDTO::fromArray($request->all());
            $action->execute($dto);
        } catch (\Exception $exception) {
            return redirect()->back();
        }
        Alert::success('Success Title', 'Тингловчи маълумотлари мувофаққиятли ўзгартирилди');
        return redirect()->route('student.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    /**
     * @param Student $student
     * @return \Illuminate\Http\RedirectResponse
     */
    public function active(Student $student)
    {
        DB::table('students')
            ->where('id', '=', $student->id)
            ->update([
                'active' => 0
            ]);
        Alert::success('Success Title', 'Тингловчи актив ҳолатдан олиб ташланди');
        return redirect()->route('student.index');
    }

    public function activeGroupStudent(Student $student)
    {
        DB::table('students')
            ->where('id', '=', $student->id)
            ->update([
                'active' => 0
            ]);
        Alert::success('Success Title', 'Тингловчи актив ҳолатдан олиб ташланди');
        return redirect()->route('group.itemStudent', $student->group_id);
    }

    public function noactive(Student $student)
    {
        DB::table('students')
            ->where('id', '=', $student->id)
            ->update([
                'active' => 1
            ]);
        Alert::success('Success Title', 'Тингловчи қайта фаоллаштирилди');
        return redirect()->route('student.index');
    }

    public function noactiveGroupStudent(Student $student)
    {
        DB::table('students')
            ->where('id', '=', $student->id)
            ->update([
                'active' => 1
            ]);
        Alert::success('Success Title', 'Тингловчи қайта фаоллаштирилди');
        return redirect()->route('group.itemStudent', $student->group_id);
    }

    public function plastik(Student $student)
    {
        return view('admin.types.plastik', compact('student'));
    }

    public function certi(Student $student)
    {
        if ($student->guvohnoma_id == 1) {
            return view('admin.types.tur_1', compact('student'));
        } else if ($student->guvohnoma_id == 2) {
            return view('admin.types.tur_2', compact('student'));
        } else if ($student->guvohnoma_id == 3) {
            return view('admin.types.tur_3', compact('student'));
        } else if ($student->guvohnoma_id == 4) {
            return view('admin.types.tur_4', compact('student'));
        } else if ($student->guvohnoma_id == 5) {
            return view('admin.types.tur_5', compact('student'));
        } else if ($student->guvohnoma_id == 6) {
            return view('admin.types.tur_6', compact('student'));
        } else if ($student->guvohnoma_id == 7) {
            return view('admin.types.tur_7', compact('student'));
        } else if ($student->guvohnoma_id == 8) {
            return view('admin.types.tur_8', compact('student'));
        } else if ($student->guvohnoma_id == 9) {
            return view('admin.types.tur_9', compact('student'));
        } else if ($student->guvohnoma_id == 10) {
            return view('admin.types.tur_10', compact('student'));
        } else if ($student->guvohnoma_id == 11) {
            return view('admin.types.tur_11', compact('student'));
        } else if ($student->guvohnoma_id == 12) {
            return view('admin.types.tur_12', compact('student'));
        } else if ($student->guvohnoma_id == 13) {
            return view('admin.types.tur_13', compact('student'));
        } else if ($student->guvohnoma_id == 14) {
            return view('admin.types.tur_14', compact('student'));
        } else if ($student->guvohnoma_id == 15) {
            return view('admin.types.tur_15', compact('student'));
        } else if ($student->guvohnoma_id == 16) {
            return view('admin.types.tur_16', compact('student'));
        } else if ($student->guvohnoma_id == 17) {
            return view('admin.types.tur_17', compact('student'));
        } else if ($student->guvohnoma_id == 18) {
            return view('admin.types.tur_18', compact('student'));
        } else if ($student->guvohnoma_id == 19) {
            return view('admin.types.tur_19', compact('student'));
        } else if ($student->guvohnoma_id == 20) {
            return view('admin.types.tur_20', compact('student'));
        }

    }
}
