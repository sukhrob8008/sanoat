<?php

namespace App\Http\Controllers\Admin;

use App\Domain\Admin\Courses\Actions\StoreCourseAction;
use App\Domain\Admin\Courses\Actions\UpdateCourseAction;
use App\Domain\Admin\Courses\DTO\StoreCourseDTO;
use App\Domain\Admin\Courses\DTO\UpdateCourseDTO;
use App\Domain\Admin\Courses\Models\Course;
use App\Domain\Admin\Courses\Repositories\CourseRepository;
use App\Domain\Admin\Courses\Requests\StoreCourseRequest;
use App\Domain\Admin\Courses\Requests\UpdateCourseRequest;
use App\Domain\Admin\Directs\Models\Yonalish;
use App\Domain\Admin\Groups\Actions\StoreGroupAction;
use App\Domain\Admin\Groups\DTO\StoreGroupDTO;
use App\Domain\Admin\Groups\Models\Group;
use App\Domain\Admin\Groups\Requests\StoreGroupRequest;
use App\Domain\Admin\Guvohnoma\Models\Guvohnoma;
use App\Domain\Admin\Students\Actions\StoreStudentAction;
use App\Domain\Admin\Students\DTO\StoreStudentDTO;
use App\Domain\Admin\Students\Requests\StoreStudentRequest;
use App\Http\Controllers\Controller;
use Illuminate\Contracts\Foundation\Application;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Http\Request;
use RealRashid\SweetAlert\Facades\Alert;

class CourseController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Application|Factory|View|\Illuminate\Http\Response
     */
    public function index(CourseRepository $courseRepository)
    {
//        $courses = $courseRepository->getPaginate();
        $courses = $courseRepository->getAll();
        return view('admin.courses.index', compact('courses'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Application|Factory|View|\Illuminate\Http\Response
     */
    public function create()
    {
        $study_plans = Yonalish::all();
        return view('admin.courses.create', compact('study_plans'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return Application|\Illuminate\Http\RedirectResponse|\Illuminate\Http\Response|\Illuminate\Routing\Redirector
     */
    public function store(StoreCourseRequest $request, StoreCourseAction $action)
    {
        try {
            $dto = StoreCourseDTO::fromArray($request->all());
            $action->execute($dto);
        } catch (\Exception $exception) {
            return redirect()->back();
        }
        Alert::success('Success Title', 'Йўналиш мувофаққиятли яратилди');
        return redirect()->route('course.index');
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * @param Course $course
     * @return Application|Factory|View|void
     */
    public function edit(Course $course)
    {
        $plans = Yonalish::all();
        return view('admin.courses.edit', compact('course', 'plans'));
    }

    /**
     * @param UpdateCourseRequest $request
     * @param Course $course
     * @param UpdateCourseAction $action
     * @return Application|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector|void
     */
    public function update(UpdateCourseRequest $request, Course $course, UpdateCourseAction $action)
    {
        try {
            $request->merge([
                'course' => $course
            ]);
            $dto = UpdateCourseDTO::fromArray($request->all());
            $action->execute($dto);
        } catch (\Exception $exception) {
            return redirect()->back();
        }
        Alert::success('Success Title', 'Йўналиш мувофаққиятли ўзгартирилди');
        return redirect()->route('course.index');
    }

    /**
     * @param Course $course
     * @return \Illuminate\Http\RedirectResponse
     */
    public function destroy(Course $course)
    {
        $course->delete();
        return redirect()->route('course.index');
    }

    /**
     * @param Course $course
     * @return Application|Factory|View
     */
    public function course_item(Course $course)
    {
        $groups = Group::where('course_id', '=', $course->id)
            ->orderBy('id', 'desc')
            ->get();
        return view('admin.groups.groups_show', compact('groups', 'course'));
    }

    public function createGroupCourse(Course $course)
    {
        return view('admin.courses.createGroupCourse', compact('course'));
    }

    public function createGroupStore(StoreGroupRequest $request, StoreGroupAction $action)
    {
        try {
            $dto = StoreGroupDTO::fromArray($request->all());
            $action->execute($dto);
        } catch (\Exception $exception) {
            return redirect()->back();
        }
        Alert::success('Success Title', 'Гуруҳ мувофаққиятли яратилди');
        return redirect()->route('course.itemGroup', $request->course);
    }

    public function createGroupCourseStudent(Group $group)
    {
        $guvohnomas = Guvohnoma::all();
        return view('admin.courses.createGroupCourseStudent', compact('group','guvohnomas'));
    }

    public function createGroupCourseStudentStore(StoreStudentRequest $request, StoreStudentAction $action)
    {
        try {
            $dto = StoreStudentDTO::fromArray($request->all());
            $action->execute($dto);
        } catch (\Exception $exception) {
            return redirect()->back();
        }
        Alert::success('Success Title', 'Тингловчи мувофаққиятли яратилди');
        return redirect()->route('group.itemStudent',$request->group_id);
    }
}
