<?php

namespace App\Domain\Admin\Courses\DTO;

class StoreCourseDTO
{

    private int $stude_plan_id;
    private string $course_title;
    private int $amaliy_soat;
    private int $nazariy_soat;

    public static function fromArray(array $data)
    {
        $dto = new self();
        $dto->setStudePlanId($data['stude_plan_id']);
        $dto->setCourseTitle($data['course_title']);
        $dto->setAmaliySoat($data['amaliy_soat']);
        $dto->setNazariySoat($data['nazariy_soat']);
        return $dto;
    }


    /**
     * @return int
     */
    public function getStudePlanId(): int
    {
        return $this->stude_plan_id;
    }

    /**
     * @param int $stude_plan_id
     */
    public function setStudePlanId(int $stude_plan_id): void
    {
        $this->stude_plan_id = $stude_plan_id;
    }

    /**
     * @return string
     */
    public function getCourseTitle(): string
    {
        return $this->course_title;
    }

    /**
     * @param string $course_title
     */
    public function setCourseTitle(string $course_title): void
    {
        $this->course_title = $course_title;
    }

    /**
     * @return int
     */
    public function getAmaliySoat(): int
    {
        return $this->amaliy_soat;
    }

    /**
     * @param int $amaliy_soat
     */
    public function setAmaliySoat(int $amaliy_soat): void
    {
        $this->amaliy_soat = $amaliy_soat;
    }

    /**
     * @return int
     */
    public function getNazariySoat(): int
    {
        return $this->nazariy_soat;
    }

    /**
     * @param int $nazariy_soat
     */
    public function setNazariySoat(int $nazariy_soat): void
    {
        $this->nazariy_soat = $nazariy_soat;
    }



}
