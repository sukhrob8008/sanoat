<?php

namespace App\Domain\Admin\Courses\Actions;

use App\Domain\Admin\Courses\DTO\UpdateCourseDTO;
use Illuminate\Support\Facades\DB;

class UpdateCourseAction
{
    public function execute(UpdateCourseDTO $updateCourseDTO)
    {
        DB::beginTransaction();
        try {
            $courses = $updateCourseDTO->getCourse();
            $courses->stude_plan_id = $updateCourseDTO->getStudePlanId();
            $courses->course_title = $updateCourseDTO->getCourseTitle();
            $courses->amaliy_soat = $updateCourseDTO->getAmaliySoat();
            $courses->nazariy_soat = $updateCourseDTO->getNazariySoat();
            $courses->update();
        }catch (\Exception $exception)
        {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
        return $courses;
    }
}
