<?php

namespace App\Domain\Admin\Courses\Actions;

use App\Domain\Admin\Courses\DTO\StoreCourseDTO;
use App\Domain\Admin\Courses\Models\Course;
use Illuminate\Support\Facades\DB;

class StoreCourseAction
{
    public function execute(StoreCourseDTO $storeCourseDTO)
    {
        DB::beginTransaction();
        try {
            $courses = new Course();
            $courses->stude_plan_id = $storeCourseDTO->getStudePlanId();
            $courses->course_title = $storeCourseDTO->getCourseTitle();
            $courses->amaliy_soat = $storeCourseDTO->getAmaliySoat();
            $courses->nazariy_soat = $storeCourseDTO->getNazariySoat();
            $courses->save();
        }catch (\Exception $exception)
        {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
        return $courses;
    }
}
