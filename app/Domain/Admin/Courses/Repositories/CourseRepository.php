<?php

namespace App\Domain\Admin\Courses\Repositories;

use App\Domain\Admin\Courses\Models\Course;

class CourseRepository
{
    public function getPaginate(){
        return Course::paginate();
    }
    public function getAll()
    {
        return Course::all();
    }
}
