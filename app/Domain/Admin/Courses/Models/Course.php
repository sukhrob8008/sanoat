<?php

namespace App\Domain\Admin\Courses\Models;

use App\Domain\Admin\Directs\Models\Yonalish;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use HasFactory;
    protected $perPage = 10;

    public function showStudyPlan()
    {
        return $this->belongsTo(Yonalish::class, 'stude_plan_id', 'id');
    }
}
