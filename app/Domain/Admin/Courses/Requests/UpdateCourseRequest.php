<?php

namespace App\Domain\Admin\Courses\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateCourseRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'stude_plan_id'=>'required',
            'course_title'=>'required',
            'amaliy_soat'=>'required',
            'nazariy_soat'=>'required',
        ];
    }
}
