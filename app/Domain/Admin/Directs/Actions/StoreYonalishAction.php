<?php

namespace App\Domain\Admin\Directs\Actions;

use App\Domain\Admin\Directs\DTO\StoreYonalishDTO;
use App\Domain\Admin\Directs\Models\Yonalish;
use Illuminate\Support\Facades\DB;

class StoreYonalishAction
{
    public function execute(StoreYonalishDTO $storeYonalishDTO)
    {
        DB::beginTransaction();
        try {
            $plans = new Yonalish();
            $plans->plan_title = $storeYonalishDTO->getPlanTitle();
            $plans->save();
        }catch (\Exception $exception)
        {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
        return $plans;
    }
}
