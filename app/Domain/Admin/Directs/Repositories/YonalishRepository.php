<?php

namespace App\Domain\Admin\Directs\Repositories;

use App\Domain\Admin\Directs\Models\Yonalish;

class YonalishRepository
{
    public function getAll()
    {
        return Yonalish::all();
    }
}
