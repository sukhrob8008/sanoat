<?php

namespace App\Domain\Admin\Guvohnoma\DTO;

class StoreGuvohnomaDTO
{
    private string $tur_title;

    public static function fromArray(array $data)
    {
        $dto = new self();
        $dto->setTurTitle($data['tur_title']);
        return $dto;
    }

    /**
     * @return string
     */
    public function getTurTitle(): string
    {
        return $this->tur_title;
    }

    /**
     * @param string $tur_title
     */
    public function setTurTitle(string $tur_title): void
    {
        $this->tur_title = $tur_title;
    }


}
