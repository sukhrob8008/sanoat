<?php

namespace App\Domain\Admin\Guvohnoma\DTO;

use App\Domain\Admin\Guvohnoma\Models\Guvohnoma;

class UpdateGuvohnomaDTO
{

    private string $tur_title;
    private Guvohnoma $guvohnoma;

    public static function fromArray(array $data)
    {
        $dto = new self();
        $dto->setTurTitle($data['tur_title']);
        $dto->setGuvohnoma($data['guvohnoma']);
        return $dto;
    }

    /**
     * @return string
     */
    public function getTurTitle(): string
    {
        return $this->tur_title;
    }

    /**
     * @param string $tur_title
     */
    public function setTurTitle(string $tur_title): void
    {
        $this->tur_title = $tur_title;
    }




    /**
     * @return Guvohnoma
     */
    public function getGuvohnoma(): Guvohnoma
    {
        return $this->guvohnoma;
    }

    /**
     * @param Guvohnoma $guvohnoma
     */
    public function setGuvohnoma(Guvohnoma $guvohnoma): void
    {
        $this->guvohnoma = $guvohnoma;
    }






}
