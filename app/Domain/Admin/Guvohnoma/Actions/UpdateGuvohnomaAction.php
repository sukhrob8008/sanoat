<?php

namespace App\Domain\Admin\Guvohnoma\Actions;

use App\Domain\Admin\Guvohnoma\DTO\UpdateGuvohnomaDTO;
use Illuminate\Support\Facades\DB;

class UpdateGuvohnomaAction
{
    public function execute(UpdateGuvohnomaDTO $updateGuvohnomaDTO)
    {
        DB::beginTransaction();
        try {
            $guvohnoma = $updateGuvohnomaDTO->getGuvohnoma();
            $guvohnoma->tur_title = $updateGuvohnomaDTO->getTurTitle();
            $guvohnoma->update();
        }catch (\Exception $exception)
        {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
        return $guvohnoma;
    }
}
