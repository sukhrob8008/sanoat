<?php

namespace App\Domain\Admin\Guvohnoma\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Guvohnoma extends Model
{
    use HasFactory;
}
