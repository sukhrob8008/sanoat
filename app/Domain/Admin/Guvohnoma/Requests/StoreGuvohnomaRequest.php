<?php

namespace App\Domain\Admin\Guvohnoma\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreGuvohnomaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'tur_title'=>'required'
        ];
    }
}
