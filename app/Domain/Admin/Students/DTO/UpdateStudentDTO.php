<?php

namespace App\Domain\Admin\Students\DTO;

use App\Domain\Admin\Students\Models\Student;
use Illuminate\Http\UploadedFile;

class UpdateStudentDTO
{
    private string $student_fio;
    private int $group_id;
    private string $malaka;
    private int $guvohnoma_id;
    private ?string $razryad=null;
    private string $pasport_seria;
    private string $pasport_number;
    private int $amaliy_baho;
    private int $nazariy_baho;
    private string $profession;
    private ?UploadedFile $student_img=null;
    private Student $student;

    public static function fromArray(array $data)
    {
        $dto = new self();
        $dto->setStudentFio($data['student_fio']);
        $dto->setGroupId($data['group_id']);
        $dto->setMalaka($data['malaka']);
        $dto->setGuvohnomaId($data['guvohnoma_id']);
        $dto->setRazryad(isset($data['razryad']) ? $data['razryad']:'-');
        $dto->setPasportSeria($data['pasport_seria']);
        $dto->setPasportNumber($data['pasport_number']);
        $dto->setAmaliyBaho($data['amaliy_baho']);
        $dto->setNazariyBaho($data['nazariy_baho']);
        $dto->setProfession($data['profession']);
        $dto->setStudentImg(isset($data['student_img']) ? $data['student_img'] : null);
        $dto->setStudent($data['student']);
        return $dto;
    }

    /**
     * @return string
     */
    public function getStudentFio(): string
    {
        return $this->student_fio;
    }

    /**
     * @param string $student_fio
     */
    public function setStudentFio(string $student_fio): void
    {
        $this->student_fio = $student_fio;
    }

    /**
     * @return int
     */
    public function getGroupId(): int
    {
        return $this->group_id;
    }

    /**
     * @param int $group_id
     */
    public function setGroupId(int $group_id): void
    {
        $this->group_id = $group_id;
    }

    /**
     * @return string
     */
    public function getMalaka(): string
    {
        return $this->malaka;
    }

    /**
     * @param string $malaka
     */
    public function setMalaka(string $malaka): void
    {
        $this->malaka = $malaka;
    }

    /**
     * @return int
     */
    public function getGuvohnomaId(): int
    {
        return $this->guvohnoma_id;
    }

    /**
     * @param int $guvohnoma_id
     */
    public function setGuvohnomaId(int $guvohnoma_id): void
    {
        $this->guvohnoma_id = $guvohnoma_id;
    }

    /**
     * @return string|null
     */
    public function getRazryad(): ?string
    {
        return $this->razryad;
    }

    /**
     * @param string|null $razryad
     */
    public function setRazryad(?string $razryad): void
    {
        $this->razryad = $razryad;
    }



    /**
     * @return string
     */
    public function getPasportSeria(): string
    {
        return $this->pasport_seria;
    }

    /**
     * @param string $pasport_seria
     */
    public function setPasportSeria(string $pasport_seria): void
    {
        $this->pasport_seria = $pasport_seria;
    }

    /**
     * @return string
     */
    public function getPasportNumber(): string
    {
        return $this->pasport_number;
    }

    /**
     * @param string $pasport_number
     */
    public function setPasportNumber(string $pasport_number): void
    {
        $this->pasport_number = $pasport_number;
    }

    /**
     * @return int
     */
    public function getAmaliyBaho(): int
    {
        return $this->amaliy_baho;
    }

    /**
     * @param int $amaliy_baho
     */
    public function setAmaliyBaho(int $amaliy_baho): void
    {
        $this->amaliy_baho = $amaliy_baho;
    }

    /**
     * @return int
     */
    public function getNazariyBaho(): int
    {
        return $this->nazariy_baho;
    }

    /**
     * @param int $nazariy_baho
     */
    public function setNazariyBaho(int $nazariy_baho): void
    {
        $this->nazariy_baho = $nazariy_baho;
    }

    /**
     * @return string
     */
    public function getProfession(): string
    {
        return $this->profession;
    }

    /**
     * @param string $profession
     */
    public function setProfession(string $profession): void
    {
        $this->profession = $profession;
    }

    /**
     * @return UploadedFile|null
     */
    public function getStudentImg(): ?UploadedFile
    {
        return $this->student_img;
    }

    /**
     * @param UploadedFile|null $student_img
     */
    public function setStudentImg(?UploadedFile $student_img): void
    {
        $this->student_img = $student_img;
    }

    /**
     * @return Student
     */
    public function getStudent(): Student
    {
        return $this->student;
    }

    /**
     * @param Student $student
     */
    public function setStudent(Student $student): void
    {
        $this->student = $student;
    }
}
