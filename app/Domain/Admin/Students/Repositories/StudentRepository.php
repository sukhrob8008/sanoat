<?php

namespace App\Domain\Admin\Students\Repositories;

use App\Domain\Admin\Students\Models\Student;

class StudentRepository
{
    public function getAll()
    {
        return Student::all();
    }
}
