<?php

namespace App\Domain\Admin\Students\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreStudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'student_fio'=>'required',
            'group_id'=>'required',
            'malaka'=>'required',
            'guvohnoma_id'=>'required',
            'pasport_seria'=>'required',
            'pasport_number'=>'required',
            'amaliy_baho'=>'required',
            'nazariy_baho'=>'required',
            'profession'=>'required',
            'student_img'=>'max:1500',
        ];
    }
}
