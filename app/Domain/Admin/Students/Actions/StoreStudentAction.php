<?php

namespace App\Domain\Admin\Students\Actions;

use App\Domain\Admin\Groups\Models\Group;
use App\Domain\Admin\Students\DTO\StoreStudentDTO;
use App\Domain\Admin\Students\Models\Student;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Storage as StorageAlias;
use Intervention\Image\Facades\Image as InterventionImage;
use SimpleSoftwareIO\QrCode\Facades\QrCode;

class StoreStudentAction
{
    public function execute(StoreStudentDTO $storeStudentDTO)
    {
        DB::beginTransaction();
        try {
            $students = new Student();
            $group = Group::query()
                ->find($storeStudentDTO->getGroupId());

            $student = Student::query()
                ->orderByDesc('number')
                ->latest()
                ->first();

            $current_student = Student::query()
                ->whereHas('group_name', function ($query) use ($group) {
                    $query->where('end_date','like','%'.substr($group->end_date, 6, 4).'%');
                })
                ->orderByDesc('number')
                ->latest()
                ->first();

            if (substr($student->created_at, 0, 4) < date('Y') && substr($student->created_at, 0, 4) != date('Y') && substr($group->end_date, 6, 4) == date('Y')) {
                $students->number = 1;
            }
            if (substr($group->end_date, 6, 4) < date('Y')) {
//                dump("eski", $current_student);
                $students->number = $current_student->number + 1;
            }
            if (substr($group->end_date, 6, 4) == date('Y')) {
//                dump("yangi", $current_student);
                if($current_student == null){
                    $students->number = 1;
                }else{
                    $students->number = $current_student->number + 1;
                }
            }


            $students->student_fio = $storeStudentDTO->getStudentFio();
            $students->group_id = $storeStudentDTO->getGroupId();
            $students->malaka = $storeStudentDTO->getMalaka();
            $students->guvohnoma_id = $storeStudentDTO->getGuvohnomaId();
            $students->razryad = $storeStudentDTO->getRazryad();
            $students->pasport_seria = $storeStudentDTO->getPasportSeria();
            $students->pasport_number = $storeStudentDTO->getPasportNumber();
            $students->amaliy_baho = $storeStudentDTO->getAmaliyBaho();
            $students->nazariy_baho = $storeStudentDTO->getNazariyBaho();
            $students->profession = $storeStudentDTO->getProfession();
            $qrImageName = time() . '.svg';
            $students->qr_name = $qrImageName;
            if ($storeStudentDTO->getStudentImg()) {
                //get filename with extension
                $filenamewithextension = $storeStudentDTO->getStudentImg()->getClientOriginalName();

                //get filename without extension
                $filename = pathinfo($filenamewithextension, PATHINFO_FILENAME);

                //get file extension
                $extension = $storeStudentDTO->getStudentImg()->getClientOriginalExtension();

                //filename to store
                $filenametostore = uniqid() . '.' . $extension;
                $storeStudentDTO->getStudentImg()->move('images/students', $filenametostore);
                //Crop image here
                $cropimage = public_path('images/students/') . $filenametostore;
                InterventionImage::make($cropimage)->crop(request()->input('w'), request()->input('h'), request()->input('x1'), request()->input('y1'))->save($cropimage);

//                dd(file_get_contents($cropimage));
//                StorageAlias::disk('google')->put('/'.$filenametostore, file_get_contents($cropimage));
//                $students->student_img = StorageAlias::disk('google')->url($filenametostore);

                $students->student_img = $filenametostore;

//                File::delete($cropimage);
            } else {
                $students->student_img = null;
            }

            $students->save();
        } catch (\Exception $exception) {
            dd($exception);
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
        return $students;
    }
}
