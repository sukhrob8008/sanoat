<?php

namespace App\Domain\Admin\Groups\Repositories;

use App\Domain\Admin\Groups\Models\Group;

class GroupRepository
{
    public function getAll()
    {
        return Group::orderBy('id','desc')->get();
    }
}
