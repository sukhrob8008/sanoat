<?php

namespace App\Domain\Admin\Groups\Actions;

use App\Domain\Admin\Groups\DTO\UpdateGroupDTO;
use Illuminate\Support\Facades\DB;

class UpdateGroupAction
{
    public function execute(UpdateGroupDTO $updateGroupDTO)
    {
        DB::beginTransaction();
        try {
            $groups = $updateGroupDTO->getGroup();
            $groups->group_title = $updateGroupDTO->getGroupTitle();
            $groups->course_id = $updateGroupDTO->getCourseId();
            $groups->start_date = date("d.m.Y", strtotime($updateGroupDTO->getStartDate()));
            $groups->end_date = date("d.m.Y", strtotime($updateGroupDTO->getEndDate()));
            $groups->teacher_one = $updateGroupDTO->getTeacherOne();
            $groups->teacher_two = $updateGroupDTO->getTeacherTwo();
            $groups->organization = $updateGroupDTO->getOrganization();
            $groups->update();
        }catch (\Exception $exception)
        {
            DB::rollBack();
            throw $exception;
        }
        DB::commit();
        return $groups;
    }
}
