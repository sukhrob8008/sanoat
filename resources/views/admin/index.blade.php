@extends('admin.layouts.master')
@section('title', 'Admin panel')
@section('content')
    <div class="container-fluid">
        <div class="row">

            <div class="col-xl-12">
                <div class="card">
                    <div class="card-header">
                        <h5>Bar chart 2</h5>
                    </div>
                    <div class="card-body">
                        <div class="apache-cotainer-large" id="echart-bar2"></div>
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection
