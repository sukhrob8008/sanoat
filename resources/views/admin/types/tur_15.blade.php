<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Document</title>
</head>
<body>
<textarea id="cer">
<main class='main'>
    <div style="height:290;" class='row1 '>
        <p class='title1' style='text-align:center;font-weight:bold'>НОУ«Саноат техника универсал сервис» г.Бухара</p>
        <div class='row-2' >
            <div class='img'></div>
            <div class='ml-auto ml'>
                <p class='title'  > <b class='title3'>УДОСТОВЕРЕНИЕ  </b> <b class='title3'  style='font-size: 18pt; color: red'> №{{sprintf("%04d", $student->number)}}</b></p>
                <p  class='font4' style='font-size:13pt'> Выдано:<b class='font' >{{$student->student_fio}} </b><br>  В том, что он(а) с {{$student->group_name->start_date}} до {{$student->group_name->end_date}} прошел(а)  курсы теоретического обучения в объеме</p>
            </div>
        </div>
        <p class='font4' style='font-size:13pt'> 36 часов  по подготовке член  постоянно-действующей   комиссии  предприятия  по  охране  труда, технике  безопасности, пожарной безопасности,  электробезопасности.</b> </p>
    </div>
    <div style="height:290;" class='row1 '  >
        <p class='font5' style='text-align: justify; font-size:1.1em'><b style='font-size:1.1em'>{{$student->student_fio}}</b> прошел(а) проверку  знаний  и  решением   квалификационной  комиссии <b style='font-size:1em'>{{$student->student_fio}}</b> присвоена квалификация  член постоянно-действующей  комиссии  предприятия  по  охране труда, технике  безопасности.<br> Основание: протокол квлф. комиссии<br> <b>№{{$student->group_name->group_title}} от {{$student->group_name->end_date}} </b></p>
        <!--                 <p class='font'></b></p>--><br>
        <p  ><b>Председатель квалиф  <br> комиссии ________________________________</b>  </p><br>
        <p  ><b>Инспектор  _______________________________</b>  </p>
        <p  ><b>Директор учебного заведения<br>   ___________________________________________ </b>    </p>
        <div class='top'>
            <img width='90' height='120' src='{{asset('images/qrcodes/'.$student->qr_name)}}'/>
        </div></main>    </div>
</main>
<br>
<main class='main'>
    <div style="height:290;" class='row1 '>
        <p class='title5' style="font-size:1em">«Sanoat texnika universal servis» <br> Non-State  Education  Institut</p>
        <div class='row-2' >
            <div class='img'></div>
            <div class='ml-auto ml'> <br>
                <p class='title'  > <b class='title3'>CERTIFICATE </b> <b class='title3'  style='color: red'> №{{sprintf("%04d", $student->number)}}</b></p>
                <p  class='title5' style='text-align: center'> <b class='font' style='font-size:1.5em'>{{$student->student_fio}} </b> <br></p>
                <p  class='font4'> To certify  that (s)he from {{$student->group_name->start_date}} to {{$student->group_name->end_date}} y. </p>
            </div>
        </div>
        <p class='font4'  style="font-size:16px;">Has passed  courses  of,  theoretical  training  of  36   academic  hours  on  preparation  of  members  of  Permanently  acting Commission    of Committee  for  labour  safety,  safety  precautions.</b> </p>
    </div>
    <div style="height:290;" class='row1 '  >
        <p class='font5' style='text-align: justify;'><b style='font-size:1.1em'>  {{$student->student_fio}}</b> as passed   examination  and  according  to  decision   of  qualifying
            commission   <b style='font-size:0.9em'>{{$student->student_fio}}</b> awarded qualification of  men Permanently  acting  commission  of  Committee  for labour safely,
            safety  precautions,  fire  safety, electric  security. <br>
            Basis: report  of  the commission <br>№{{$student->group_name->group_title}} {{$student->group_name->end_date}} y.</p>
        <p  ><b>Chairman   of  the qualifying  commission __________________________________________</b>  </p>
        <p  ><b>Director of educational institution<br>   ___________________________________________ </b>    </p>
        <div class='top'>
            <img width='90' height='120' src='{{asset('images/qrcodes/'.$student->qr_name)}}'/>
        </div></main>
    </div>
</main>
</textarea>
<script src="https://cdn.tiny.cloud/1/yhkqwyog2j2wp3tbr92cd19v1i7o4x4na65vcbndlhf5tjl9/tinymce/7/tinymce.min.js" referrerpolicy="origin"></script>

<script>
    tinymce.init({
        selector: '#cer',
        height: 800,
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste pagebreak"
        ],
        toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image pagebreak',
        toolbar_mode: 'floating',
        tinycomments_mode: 'embedded',
        tinycomments_author: 'Author name',
        pagebreak_split_block: true,
        content_css: ["{{ asset('styles/assets/guvohnoma/style_15_tur.css') }}"]
    });
</script>

</body>
</html>
