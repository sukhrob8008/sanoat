<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Document</title>
</head>
<body>
<textarea id="cer">
<main class='main'>
    <div class='row1'>
        <p class='title1' style='text-align:center;font-weight:bold'>НОУ«Саноат техника универсал сервис» г.Бухара</p>
        <div class='row-2' >
            <div class='img'></div>
            <div class='ml-auto ml'>
                <p class='title2'  > <b class='title3' style='font-size: 1.2em;'>УДОСТОВЕРЕНИЕ  </b> <b class='title3'  style='color: red; font-size: 1.2em;'> №{{sprintf("%04d", $student->number)}}</b></p>
                <p class='font2' style='font-size: 1.4em;'> Выдано: <b class='font2' style='font-size: 1.1em;'>  {{$student->student_fio}} </b>
                    <br> в том, что он (а) с <br>   {{$student->group_name->start_date}}   по  {{$student->group_name->end_date}} года  </p>
            </div>
        </div>
        <p class='font' style='font-size: 1.3em; text-align: center;'>обучался на курсах <br> <b class='font' style='font-size: 0.9em; text-align: center;'> НОУ «Саноат техника универсал сервис» </b> <br> по специальности <b class='font' style='font-size: 0.9em; text-align: center;'>  {{$student->malaka}} </b> <br><p>

    </div>
    <div class='row1'>
        <p class='center'><strong class='title2'>{{$student->student_fio}} </strong></p>
        <span style='font-size: 11px; text-align: center;'> (лицо прошедшее обучения)</span>
        <p class='font3'>Решением квалификационной комиссии</p>
        <p class='font3'><strong class='font' style='font-size: 0.9em; text-align: center;'> НОУ&laquo;Саноат техника универсал сервис&raquo; </strong></p>
        <p class='font'>присвоена квалификации: <b class='font1'> {{$student->malaka}}</b> Основание Протокол&nbsp;квлф. комиссии&nbsp; <br> № {{$student->group_name->group_title}} от {{$student->group_name->end_date}} года</p>
        <p class='font' style='font-size:1.1em'>Председатель квалификационной <br> комиссии__________________________ </p>
        <p><strong> </strong></p>
        <p class='font' style='font-size:1.1em'>Директор учебного заведения <br> __________________________________ </p>
        <p><strong> </strong></p>
        <div class='top'>
            <img width='90' height='120' src='{{asset('images/qrcodes/'.$student->qr_name)}}'/>
        </div></main>
</textarea>
<script src="https://cdn.tiny.cloud/1/yhkqwyog2j2wp3tbr92cd19v1i7o4x4na65vcbndlhf5tjl9/tinymce/7/tinymce.min.js" referrerpolicy="origin"></script>

<script>
    tinymce.init({
        selector: '#cer',
        height: 800,
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste pagebreak"
        ],
        toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image pagebreak',
        toolbar_mode: 'floating',
        tinycomments_mode: 'embedded',
        tinycomments_author: 'Author name',
        pagebreak_split_block: true,
        content_css: ["{{ asset('styles/assets/guvohnoma/style_10_tur.css') }}"]
    });
</script>

</body>
</html>
