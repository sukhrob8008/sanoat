<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Document</title>
</head>
<body>
<textarea id="cer">
{{--<main class='main'>--}}
    {{--    <div class='row1'>--}}
    {{--        <p class='title1' style='text-align:center;font-weight:bold'>НОУ«Саноат техника универсал сервис» г.Бухара</p>--}}
    {{--        <div class='row-2' >--}}
    {{--            <div class='img'></div>--}}
    {{--            <div class='ml-auto ml'>--}}
    {{--                <p class='title3'  > <b class='title4'> УДОСТОВЕРЕНИЕ  </b> <b class='title4'  style='color: red'> №{{$student->id}}</b></p>--}}
    {{--                <p class='font2'  > Выдано: <b class='font2'>  {{$student->student_fio}} </b>--}}
    {{--                    <br> в том, что он (а) с <br>   {{$student->group_name->start_date}}  г.   по  {{$student->group_name->end_date}} г.   </p>--}}
    {{--            </div>--}}
    {{--        </div>--}}
    {{--        <p class='font' >прошел(а)    курсы  теоретического    обучения в объеме (20) часов промышленной безопасноси, технике безопасности, пожарной безопасности, электробезопасности и первой помощи. <p>--}}
    {{--    </div>--}}
    {{--    <div class='row1'  >--}}
    {{--        <p class='center'  ><b class='font2'> Основание протокола квалификационной комиссии  </b></p>--}}
    {{--        <p  class='font2' >№{{$student->group_name->group_title}} от {{$student->group_name->end_date}}</b>г. </p>--}}
    {{--        <p  class='font2' > <b class='font2'>{{$student->student_fio}}</b> </p>--}}
    {{--        <p class='font'>По проверке знаний требований промышленной безопасности, технике безопасности, пожарной безопасности,электробезопасности</p>--}}
    {{--        <p class='font'>и первой помощи.</p>--}}
    {{--        <p><strong style='font-size: 1em;'>Председатель квалиф <br/>комиссии _____________________________</strong></p>--}}
    {{--        <p><strong style='font-size: 1em;'>Директор учебного <br/>заведения ____________________________ </strong></p>--}}
    {{--        <div class='top'>--}}
    {{--            <img width='90' height='120' src='{{asset('images/qrcodes/'.$student->qr_name)}}'/>--}}
    {{--        </div>    </div>--}}
    {{--</main>--}}


    <div><br><br><br><br>
        <p style="text-align: center; font-size: 1.2rem;" data-mce-style="text-align: center; font-size: 1.2rem;">Меҳнатни муҳофаза қилиш соҳасида малака оширганлик ҳақида</p><br>
        <h1 style="text-align: center; color: red; font-size: 34px; font-family: 'Times New Roman';" data-mce-style="text-align: center; color: red; font-size: 34px;">СЕРТИФИКАТ <br>№{{sprintf("%04d", $student->number)}}</h1>
        <strong style="text-align: center;" data-mce-style="text-align: center;"> </strong> <br>
        <h2 style="text-align: center; font-size: 24px;" data-mce-style="text-align: center; font-size: 24px; font-family: 'Times New Roman';">{{$student->student_fio}}</h2>
        <p style="text-align: center; font-size: 1.5rem;" data-mce-style="text-align: center; font-size: 1.5rem;"><br>{{$student->group_name->start_date}} санадан {{$student->group_name->end_date}} санагача</p><br>
        <h1 style="text-align: center; font-size: 20px;" data-mce-style="text-align: center; font-size: 20px;">"САНОАТ ТЕХНИКА УНИВЕРСАЛ СЕРВИС" НОУ <br>
            <span style="font-size: 15px;" data-mce-style="font-size: 15px;"> (малака ошириш ва қайта тайёрлаш таьлим муассасасининг номи)</span>
        </h1>
        <p style="text-align: center; font-size: 1.5rem;" data-mce-style="text-align: center; font-size: 1.5rem;">нодавлат таьлим муассасасида <br>Меҳнатни муҳофаза қилиш соҳасидаги мутахассисларнинг <br>
            <span style="font-size: 15px;" data-mce-style="font-size: 15px;"> (ходимларни меҳнатни муҳофаза қилиш бўйича малакасини ошириш дастурнинг номи)</span>
        </p>
        <p style="text-align: center; font-size: 1.5rem;" data-mce-style="text-align: center; font-size: 1.5rem;">малакасини ошириш курси ўқув дастури</p>
        <p style="text-align: center; font-size: 1.5rem;" data-mce-style="text-align: center; font-size: 1.5rem;">бўйича «72» соат ҳажмида малака ошириш курсидан ўтди.</p><br><br><br><p><br></p><p><br></p><p><br></p><p><br></p>
        <p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; Муҳр ўрни:</p><br><p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Имтиҳон комиссияси раиси __________&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; Зарипов.&nbsp; Ш.&nbsp; А.</p><p><br></p>
        <p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Таьлим муассасаси раҳбари _________&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Саидов.&nbsp; A.&nbsp; A.</p><p><br></p>
        <p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Шаҳар (туман)&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Бухоро</p><p><br></p>
        <p>&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;Ташкилот серияси 017196 рестр рақами "14.12.2021" дан</p>
        <div class="top" style="max-width: 30mm; max-height: 30mm; border: 0px;" data-mce-style="max-width: 30mm; max-height: 30mm; border: 0px;">
            <span contenteditable="false" style="max-width: 30mm;max-height: 30mm; border: 0px;" data-mce-object="iframe" class="mce-preview-object mce-object-iframe" data-mce-p-title="Iframe Example" data-mce-p-src="qrcode?id=3a15c7d0bbe60300a39f76f8a5ba6896">
                <img width='90' height='120' src='{{asset('images/qrcodes/'.$student->qr_name)}}'/>
            <span class="mce-shim"></span></span>
        </div>
    </div>

</textarea>
<script src="https://cdn.tiny.cloud/1/yhkqwyog2j2wp3tbr92cd19v1i7o4x4na65vcbndlhf5tjl9/tinymce/7/tinymce.min.js" referrerpolicy="origin"></script>

<script>
    tinymce.init({
        selector: '#cer',
        height: 800,
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste pagebreak"
        ],
        toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image pagebreak',
        toolbar_mode: 'floating',
        tinycomments_mode: 'embedded',
        tinycomments_author: 'Author name',
        pagebreak_split_block: true,
        content_css: ["{{ asset('styles/assets/guvohnoma/style_9_tur.css') }}"]
    });
</script>

</body>
</html>
