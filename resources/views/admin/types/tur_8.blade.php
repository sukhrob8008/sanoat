<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Document</title>
</head>
<body>
<textarea id="cer">
<main class='main'>
    <div class='row1'>
        <p class='title2'>НОУ«Саноат техника универсал cервис» <br> г.Бухара</p>
        <div class='row-2' >
            <div class='img'></div>
            <div class='ml-auto ml'>
                <p class='title3'  > <b class='title4'> УДОСТОВЕРЕНИЕ  </b> <b class='title4'  style='color: red'>№   {{sprintf("%04d", $student->number)}}  </b></p>
                <p class='font2'  > Выдано: <b class='font2'>   {{$student->student_fio}} </b>
                    <br> в том, что он (а) с <br>   {{$student->group_name->start_date}}  г.   по  {{$student->group_name->end_date}} г.   </p>
            </div>
        </div>
        <p class='font' >прошел(а)    курсы  теоретического    обучения в объеме ({{$student->group_name->kurslar->nazariy_soat + $student->group_name->kurslar->amaliy_soat}}) часов охраны труда, технике безопасности, пожарной безопасности, электробезопасности и первой помощи. <p>
    </div>
    <div class='row1'  >
        <p class='center'  ><b class='font2'>   Основание протокола квалификационной комиссии  </b></p>
        <p  class='font2' >№ {{$student->group_name->group_title}} от {{$student->group_name->end_date}}</b>г. </p>
        <p  class='font2' > <b class='font2'>{{$student->student_fio}}</b> </p>
        <p class='font'>По проверке знаний требований охраны труда, технике безопасности, пожарной безопасности,электробезопасности</p>
        <p class='font'>и первой помощи.</p>
        <p><strong style='font-size: 1em;'>Председатель квалиф <br />комиссии _____________________________</strong></p>
        <p><strong style='font-size: 1em;'>Директор учебного <br />заведения ____________________________ </strong></p>
        <div class='top'>
            <img width='90' height='120' src='{{asset('images/qrcodes/'.$student->qr_name)}}'/>
        </div>    </div>
</main>

</textarea>
<script src="https://cdn.tiny.cloud/1/yhkqwyog2j2wp3tbr92cd19v1i7o4x4na65vcbndlhf5tjl9/tinymce/7/tinymce.min.js" referrerpolicy="origin"></script>

<script>
    tinymce.init({
        selector: '#cer',
        height: 800,
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste pagebreak"
        ],
        toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image pagebreak',
        toolbar_mode: 'floating',
        tinycomments_mode: 'embedded',
        tinycomments_author: 'Author name',
        pagebreak_split_block: true,
        content_css: ["{{ asset('styles/assets/guvohnoma/style_8_tur.css') }}"]
    });
</script>

</body>
</html>

