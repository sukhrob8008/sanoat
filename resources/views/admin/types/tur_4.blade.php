<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Document</title>
</head>
<body>
<textarea id="cer">
<main class='main'>
    <div class='row'>
        <br><br>
        <div class='row-2' >
            <div class='img'></div>
            <p class='title ' style='margin-left: 15px; font-family:  serif;'  > <br>  <b style='font-family:  serif; font-size: 1.3em;'> __________________</b><br>
            <b>(личная подпись)</b>
        <br><b><br> <b style='font-family:  serif; font-size: 1.4em;'> УДОСТОВЕРЕНИЕ </b> </b> <b class='ml'  style='color: red'> <br> <b style='font-family:  serif; font-size: 1.4em;'> №{{sprintf("%04d", $student->number)}} </b></b></p>
        </div>
    </div>
    <div class='row'  >
        <br>
        <p class='fonts'> Выдано: <b class='title-2'>  {{$student->student_fio}} </b></p>
        <p class='fonts'> в том, что он <b> {{$student->group_name->end_date}} года </b> окончил </p>
        <p  style='text-align: center;'> <b class='title-2' style='font-size: 1.4em;' > НОУ « Саноат техника универсал сервис » </b></p>
        <p class='font' style='text-align: center;margin-bottom:10px'> по профессии:  {{$student->malaka}} </p>
        <div class='top'>
            <img style='margin-top:40px' width='70' height='100' src='{{asset('images/qrcodes/'.$student->qr_name)}}'/>
        </div>
    </div>
</main>
<br>
<main class='main'>
    <div class='row'>
        <p class='font-1'>К удостоверению № ____</p>
        <p class='font-1'><strong>Переквалификация № ____</strong></p>
        <p class='font-1'>Решением квалификационной комиссии</p>
        <p class='font-1'>Гр___________________________________ присвоена квалификация _______________</p>
        <p class='font-1'>разряд (класс категория)________________</p>
        <p class='font-1'>допускается к обслуживанию____________</p>
        <p><strong class='font-1'>Протокол № _____</strong> '___' ____________ _____ г</p>
        <p class='font-1'>М.П.<br />Председатель Квалиф. Комиссии&nbsp; &nbsp;____________________________________</p>
        <p class='font-1'>Инспектор ___________________________</p>
    </div>
    <div class='row'>
        <p class='font-1'><strong>К удостоверению (переаттестация) № ____</strong></p>
        <p class='font-1'>Гр _________________________________ периодической проверки знаний в объёме производственной инструкции.</p>
        <p><strong class='font-1'>Протокол № _____</strong></p>
        <p class='font-1'><strong> По </strong> '___' ___________________________</p>
        <p class='font-1'><strong> До </strong> '___' ___________________________</p>
        <p class='font-1'>М.П.<br />Председатель Квалиф. Комиссии</p>
        <p class='font-1'>__________________________________</p>
        <p class='font-1'>&nbsp;</p>
        <p class='font-1'>Инспектор_________________________</p>
    </div>
</main>
<div style='page-break-before: always; clear:both'></div>
<main class='main'>

    <div class='row'><br />
        <p class='font-2' style='text-align: center;'><strong style='font-family:  serif;  font-size:0.85em'>Решением экзаменационной комиссии</strong></p>
        <p style='margin: 0px; text-align: center;'><strong class='font-1' style='font-size: 1.3em;'> {{$student->student_fio}} </strong></p>
        <p class='font-2' style='text-align: center;'>присвоена квалификация</p>
        <p class='font-2' style='text-align: center;'><strong style='font-family:  serif; font-size:0.85em'>  {{$student->malaka}} <br>  {{$student->razryad}}  <br> допускается к обслуживанию и эксплуатации грузоподъёмных автомобильных кранов с механическим и гидравлическим приводом с грузоподъёмностью 75 тн.</strong></p>
    </div>
    <div class='row'></div>
</main>
<br>
<main class='main'>
    <div class='row'>&nbsp;</div>
    <div class='row'>
        <p style='text-align: center;' class='font-2'>Основание: Протокол экзаменационной комиссии</p>
        <p style='text-align: center;'><strong class='font-2'>Протокол № {{$student->group_name->group_title}} </strong></p>
        <p class='font2'>По {{$student->group_name->end_date}} г.</p>
        <p class='font2'>Председатель экзаменационной <br /></p>
        <p class='font2'>комиссии_________________________</p>
        <p class='font2'>&nbsp;</p>
        <p class='font2'>Инспектор ________________________</p>
        <p class='font2'>&nbsp;</p>
        <p class='font2'>Директор учебного заведения_________________________</p>
    </div>

</main>
     </textarea>
<script src="https://cdn.tiny.cloud/1/yhkqwyog2j2wp3tbr92cd19v1i7o4x4na65vcbndlhf5tjl9/tinymce/7/tinymce.min.js" referrerpolicy="origin"></script>

<script>
    tinymce.init({
        selector: '#cer',
        height: 800,
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste pagebreak"
        ],
        toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image pagebreak',
        toolbar_mode: 'floating',
        tinycomments_mode: 'embedded',
        tinycomments_author: 'Author name',
        pagebreak_split_block: true,
        content_css: ["{{ asset('styles/assets/guvohnoma/style_4_tur.css') }}"]
    });
</script>

</body>
</html>
