<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Document</title>
</head>
<body>
<textarea id="cer">
<div style='page-break-before: always; clear:both'></div>
<main class='main'>
    <div class='row1'>
        <p class='font2'><strong class='font2'>К удостоверению № ____</strong></p>
        <p class='font2'>Гр ___________________________________ <br> подвергнут Периодической проверке знаний <br> в объёме производственной инструкции.</p>
        <p><strong class='font2'>Протокол № __________</strong></p>
        <p><strong class='font2'>По '__'&nbsp; _________________</strong></p>
        <p><strong class='font2'>До '__'&nbsp; _________________</strong></p>
        <p><strong class='font2'>М.П.<br />Председатель </strong></p>
        <p><strong class='font2'>Квалиф. Комиссии _____________________</strong></p>
        <p><strong class='font2'>Инспектор ______________________________</strong></p>
    </div>
    <div class='row1'></div>
</main>
<br>

</textarea>
<script src="https://cdn.tiny.cloud/1/yhkqwyog2j2wp3tbr92cd19v1i7o4x4na65vcbndlhf5tjl9/tinymce/7/tinymce.min.js" referrerpolicy="origin"></script>

<script>
    tinymce.init({
        selector: '#cer',
        height: 800,
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste pagebreak"
        ],
        toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image pagebreak',
        toolbar_mode: 'floating',
        tinycomments_mode: 'embedded',
        tinycomments_author: 'Author name',
        pagebreak_split_block: true,
        content_css: ["{{ asset('styles/assets/guvohnoma/style_4_tur.css') }}"]
    });
</script>

</body>
</html>
