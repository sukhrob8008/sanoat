<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Document</title>
</head>
<body>
<textarea id="cer">
    <br><br>
<main class='main ' style='justify-content: center; align-items: center;'>

    <div class='row' style='height:141mm'>
        <b style='font-size: 1.2em; text-align: center; font-family: sans-serif;'>Повторные  или   дополнительные   проверке </b>
        <p  style='font-size: 1.1em; font-family: sans-serif;' ><br> 2023г.Постоянно действующей  комиссией  при _____________
            <br> были проведены повторные и дополнитель-ные проверки сварщика .
            <br> <br> При проверке сваривались _____________________
            <br> из стали марки _____________________
            <br> с выполнением швов_____________________
            <br> с применением присадочого материала Вид  термообработки  образцов до испытания _______________________________________
            На основании проверке теоретических  и прак- тических  знаний сварщик получил следующие
            оценки: теоретические знания 4
            <br> практическая подготовка 4
            <br>  и  допущен к <b> __________________________</b>
        <p style='font-size: 1.3em; font-family: sans-serif;'> Удостоверение  действительно по ______________</p>
        <p  ><b style='font-size: 1.3em; font-family: sans-serif;'>Председатель  комиссии: _________________________ </b>  </p>
        <p  ><b style='font-size: 1.3em; font-family: sans-serif;'>Члены  комиссии: _________________________________     </b></p>
        </p>
    </div>
    <div class='row' style='height:141mm'>
        <p  style='font-size: 1.1em; font-family: sans-serif;'> Продлен по _______________20____г </p>
        <p style='margin-left: 20px; font-family: sans-serif;' ><b style='font-size: 1.2em;'> Председатель комиссии __________________ </b></p>
        <p style='margin-left: 20px; font-family: sans-serif;'><b style='font-size: 1.2em;'> Член комиссии __________________ </b></p>

        <p  style='font-size: 1.1em; font-family: sans-serif;'>Продлен по _______________20____г </p>
        <p style='margin-left: 20px; font-family: sans-serif;' ><b style='font-size: 1.2em;'> Председатель комиссии __________________ </b></p>
        <p style='margin-left: 20px; font-family: sans-serif;'><b style='font-size: 1.2em;'> Член комиссии __________________ </b></p>

        <p style='font-size: 1.1em; font-family: sans-serif;' >Продлен по _______________20____г </p>
        <p style='margin-left: 20px; font-family: sans-serif;' ><b style='font-size: 1.2em;'> Председатель комиссии __________________ </b></p>
        <p style='margin-left: 20px; font-family: sans-serif;'><b style='font-size: 1.2em;'>  Член комиссии __________________ </b></p>

        <p style='font-size: 1.1em; font-family: sans-serif;'>Продлен по _______________20____г </p>
        <p style='margin-left: 20px; font-family: sans-serif;' ><b style='font-size: 1.2em;'> Председатель комиссии __________________ </b></p>
        <p style='margin-left: 20px; font-family: sans-serif;'><b style='font-size: 1.2em;'> Член комиссии __________________ </b></p>

        <p style='font-size: 1.1em; font-family: sans-serif;' >Продлен по _______________20____г </p>
        <p style='margin-left: 20px; font-family: sans-serif;' ><b style='font-size: 1.2em;'> Председатель комиссии __________________ </b></p>
        <p style='margin-left: 20px; font-family: sans-serif;'><b style='font-size: 1.2em;'> Член комиссии __________________ </b></p>

        <p  style='font-size: 1.1em; font-family: sans-serif;'>Продлен по _______________20____г </p>
        <p style='margin-left: 20px; font-family: sans-serif;' ><b style='font-size: 1.2em;'> Председатель комиссии __________________ </b></p>
        <p style='margin-left: 20px; font-family: sans-serif;'><b style='font-size: 1.2em;'> Член комиссии __________________ </b></p>

        <p style='font-size: 1.1em; font-family: sans-serif;' >Продлен по _______________20____г </p>
        <p style='margin-left: 20px; font-family: sans-serif;' ><b style='font-size: 1.2em;'> Председатель комиссии __________________ </b></p>
        <p style='margin-left: 20px; font-family: sans-serif;'><b style='font-size: 1.2em;'> Член комиссии __________________ </b></p>
        <!-- <p class='margin-top: 10px font-family: sans-serif;' style='font-size: 1em;' >Печать предприятия  </p> -->
    </div>
</main>

<br><br>

<main class='main' >
    <div class='row' style='height: 146mm;'>
        <p class='title' >
            <b style='font-size: 1.1em; font-family: sans-serif;'>УДОСТОВЕРЕНИЕ  </b> <b class='ml'  style='color: red; font-size: 1em; font-family: sans-serif;'> №   {{sprintf("%04d", $student->number)}}  </b></p>
        <p style='font-size: 1.1em; font-family: sans-serif;' >Выдано гр. <b style='font-size: 1.2em; font-family: sans-serif;'>{{$student->student_fio}}</b> </p>
        <p  style='font-size: 1.1em; font-family: sans-serif;' >  ____ года  рождения , имеющему  стаж  работы по сварке    лет, в  том ,что  он   согласно Правилам  аттестации сварщиков, утвержденным «Саноатгеоконтехназорат» РУз. 20__ г. Прошел   аттестацию   постоянно  действующей комиссией при</p>

        <p  class='center title-2'><b style='font-size: 1.2em; font-family: sans-serif;'>НОУ«Саноат техника универсал сервис» </b></p>
        <p class='center' style='font-size: 1.2em; font-family: sans-serif;'> г.Бухара</p>
        <p  class='center' style='margin: 0px; font-family: sans-serif; '> <span style='font-size:12px;'> (наименование предприятия организации) по </span> </p>
        <p  class='center title-2'> <b style='font-size: 1.2em; font-family: sans-serif;'> РЭД сварке </b><p>
        <p  class='center' style='margin: 0px'> <span style='font-size:12px; font-family: sans-serif;'> (Указат. способ сварке) </span> </p>
        <p style='font-size: 1.1em; font-family: sans-serif;'>При проверке сваривались: пластины толщиной 8.00 мм , трубы диаметром __ мм, с толщиной стенки __ из основного металла </p>
        <p style='font-size: 1.1em; font-family: sans-serif;'> марки: <b style='font-size: 1.2em; font-family: sans-serif;'> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; сталь-20 с выполнением </b>  </p>
        <p style='font-size: 1.1em; font-family: sans-serif;'> швов: <b style='font-size: 1.2em; font-family: sans-serif;'> &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  стыковых </b>  </p>
        <p  class='center' style='margin: 0px'> <span style='font-size:12px; font-family: sans-serif;'> (стыковых, товаровых и др.) </span> </p>
        <p style='font-size: 1.1em; font-family: sans-serif;'> в  положениях:<b style='font-size: 1.2em; font-family: sans-serif;'>&nbsp; &nbsp; &nbsp; поворотных </b>  </p>
        <p style='font-size: 1.1em; font-family: sans-serif;'> с применением  сварочных материалов:<br>  </p>
        <p class='center'><b style='font-size: 1.2em; font-family: sans-serif;' > Э/Д УОНИ – 13/55</b></p>
    </div>
    <div class='row' style='height: 146mm;'></div>
</main><br><br>



<main class='main' style='justify-content: center; align-items: center;' >
    <div class='row' style='height: 146mm;'></div>
    <div class='row' style='height: 146mm;'>
        <p  ><b style='font-size: 1.2em; font-family: sans-serif;'> При проверке теоретических  и практических Знаний сварщик  т. </b>    </p>
        <p  class='center title-2' style='font-size: 1.3em; font-family: sans-serif;' > гр.   {{$student->student_fio}}</b> </p>
        <p style='font-size: 1.1em; text-align: center; font-family: sans-serif;'> Получил следующие оценки :</p>
        <p><b style='font-size: 1.1em; font-family: sans-serif;'>- теорические      знания:    Хорошо  </b>  </p>
        <p><b style='font-size: 1.1em; font-family: sans-serif;'>- практическая подготовка:   Хорошо   </b>   </p>
        <p  style='font-size: 1.2em; font-family: sans-serif;'> и допущен  к  <b style='font-size: 1.1em; font-family: sans-serif;'> сварке металлоконструкции во всех пространственных положениях</b> </p>
        <p  class='center' style='margin: 0px; '> <span style='font-size:10px; font-family: sans-serif;'> (способ и положение сварки,вид  работы  и  тип металла)</span> </p>
        <p style='font-size: 1.2m; text-align: center; font-family: sans-serif;' >Удостоверение  выдано на основании протокола Постоянно действующей комиссии  </p>
        <p  class='center title-2'><b style='font-size: 1.2em; font-family: sans-serif;'>НОУ«Саноат техника универсал сервис» </b></p>
        <p class='center' style='font-size: 1.2em; font-family: sans-serif;'>  г.Бухара </p>
        <p  class='center' style='font-size: 1.2em; font-family: sans-serif;' > №  {{$student->group_name->group_title}}   от {{$student->group_name->end_date}} г.</b> </p>
        <p style='font-size: 1.2em; font-family: sans-serif;' >Удостоверение действительно по _________</p>
        <p  ><b style='font-size: 1.2em; font-family: sans-serif;'>Председатель  комиссии: _____________________________________</b></p>
        <p  ><b style='font-size: 1.2em; font-family: sans-serif;'> Руководитель предариятия <br> организации: _______________________     </b></p>

        <p  ><b style='font-size: 1.2em; font-family: sans-serif;'>Печать предприятия  <br>  Личная подпись  сварщика: ____________________________________     </b></p>
        <div class='top'>
            <img width='70' height='90' src='{{asset('images/qrcodes/'.$student->qr_name)}}'/>
        </div>
    </div>
</main><br><br>
<div style='break-before: page; clear: both; line-height: 1;'>&nbsp;</div><br><br>
<main class='main3'>
    <div class='row3'>
        <p class='title'><strong style='font-size: 1em; font-family: sans-serif;'> СВИДЕТЕЛЬСТВО </strong> <strong class='ml' style='color: red; font-size: 1em; font-family: sans-serif;'> №   {{sprintf("%04d", $student->number)}}  </strong></p>
        <div class='row-2'>
            <div class='img'>&nbsp;</div>
            <p class='ml-auto ml' style='font-size: 1.4em; text-align: center; font-family: sans-serif;'>Выдано: <strong  style='font-size: 1em; font-family: sans-serif; '> {{$student->student_fio}} </strong> <br />в том, что он (а) с {{$student->group_name->start_date}} года по {{$student->group_name->end_date}} <br />года обучался (лось) по профессии</p>
        </div>
        <br>
        <p class='center'><strong style='font-size: 1.7em; font-family: sans-serif; line-height: 0px;'>{{$student->malaka}} </strong></p>
        <p class='center ' style='margin: 0px;'><span style='font-size: 13px; font-family: sans-serif; line-height: 15px;'> (наименование профессии) </span></p>
        <p class='center' style='font-family: sans-serif; font-size: 1.5em; line-height: 20px;'>{{$student->group_name->kurslar->showStudyPlan->plan_title}}  </p>
        <p class='center' style='margin: 0px;'><span style='margin: 0px; font-size: 13px; font-family: sans-serif; line-height: 10px;'> (форма обучения)</span></p>
        <p class='title-2' style='font-family: sans-serif; font-size: 1.5em;'>НОУ&laquo;Саноат техника универсал сервис&raquo; </p>
        <!--         <p class='center' style='font-family: 'Times New Roman', font-size: 1.2em, Times, serif;'>г.Бухара</p>-->
        <p class='center' style='font-size: 1em; margin: 0px; font-family: sans-serif;'><span style='font-size: 12px;'> (наименование предприятия организации) </span></p>
        <p class='center' style='font-size: 1.2em; font-family: sans-serif; text-align: justify;'>Прошел (а) полный курс теоретического обучения в объеме {{$student->group_name->kurslar->nazariy_soat}} часов и производственного обучения в объеме {{$student->group_name->kurslar->amaliy_soat}} часов и сдал (а) квалификационный экзамен с оценками:</p>
        <p><strong style='font-size: 1.2em; font-family: sans-serif;'>- теория:  {{$student->nazariy_baho}}&nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp; &nbsp;  - практика: {{$student->amaliy_baho}} </strong></p>
        <p><strong style='font-size: 1.2em; font-family: sans-serif;'></strong></p>

    </div>
    <div class='row3' style='margin-left:26px'>
        <p class='center' ><b style='font-size: 1.3em; font-family: sans-serif;'> Решением (Экзаменационной)<br> Квалификационной комиссии </b></p>
        <p  class='center' style='font-size: 1.3em; font-family: sans-serif;'  > от {{$student->group_name->end_date}}</b>  г.  Протокол № {{$student->group_name->group_title}} </p>
        <p  class='center title-2' style='font-family: sans-serif; font-size: 1.3em;' > <b style='font-size: 1.2em; font-family: sans-serif;' > гр.   {{$student->student_fio}}</b> </p> <br>
        <p style='font-family: sans-serif; font-size: 1.3em;    '> Установлен тарифно-квалификационный <b style='font-size: 1.1em;text-decoration:underline'>разряд</b> (класс категория) {{$student->razryad}}  по профессии:</p>
        <p class='center' ><b style='font-size: 1.7em; font-family: sans-serif; '>{{$student->malaka}}</b>  </p>
        <br>
        <p  ><b style='font-size: 1em; font-family: sans-serif; font-size: 1.2em;'>Председатель  квалификационной <br>комиссии:___________________________</b></p>
        <br>
        <p  ><b style='font-size: 1em; font-family: sans-serif; font-size: 1.2em;'>Руководитель  предприятия<br>Организации:________________________</b></p>
        <br>
        <p ><b  style='font-size: 18px !important; font-family: sans-serif; '>М.П.<br> НОУ«Саноат  техника  универсал сервис»</b></p>
    </div>
</main>

</textarea>
<script src="https://cdn.tiny.cloud/1/yhkqwyog2j2wp3tbr92cd19v1i7o4x4na65vcbndlhf5tjl9/tinymce/7/tinymce.min.js" referrerpolicy="origin"></script>

<script>
    tinymce.init({
        selector: '#cer',
        height: 800,
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste pagebreak"
        ],
        toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image pagebreak',
        toolbar_mode: 'floating',
        tinycomments_mode: 'embedded',
        tinycomments_author: 'Author name',
        pagebreak_split_block: true,
        content_css: ["{{ asset('styles/assets/guvohnoma/style_19_tur.css') }}"]
    });
</script>

</body>
</html>
