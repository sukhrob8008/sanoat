<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Document</title>
</head>
<body>
<textarea id="cer">
<main class='main'>
    <div class='row1 row2'>
        <p class='title2' >НОУ«Саноат техника универсал сервис» г.Бухара</p>
        <div class='row-2' >
            <div class='img'></div>
            <div class='ml-auto ml'>
                <p class='title'  > <b class='title3'>УДОСТОВЕРЕНИЕ  </b> <b class='title3'  style='color: red'> №{{sprintf("%04d", $student->number)}}</b></p>
                <p  class='font'> Выдано: <b class='font'>  {{$student->student_fio}} </b>
                    в том, что он (а) с  {{$student->group_name->start_date}}  г.   по  {{$student->group_name->end_date}} г.  обучался   на   курсах  </p>
            </div>
        </div>
        <p class='center' ><b class='font'>НОУ«Саноат техника универсал сервис»</b>  г Бухара <br> <span style='font-size:10px;'> (наименование учебного заведения ) </span></p>
        <!-- <p class='title2'><b></b><br>  <span style='font-size:10px;'> (область, город, район, ) </span> </p> -->
        <p class='font'> по специальности: <b class='font6'>{{$student->malaka}} </b> </p>
    </div>
    <div class='row1 row2'  >
        <p  class='font1' > <b class='font'>{{$student->student_fio}}</b></p>
        <p class='font1'> Решением квалификационной комиссии <br> <b class='title2'>НОУ«Саноат техника универсал cервис»</b> </p>
        <p class='font1'> присвоена квалификация: <br>  <b class='font'>{{$student->malaka}}</b>  </p>
        <p class='font'> Основание: протокол квлф. комиссии <br><b>№ {{$student->group_name->group_title}} от {{$student->group_name->end_date}} г.</b>  </p>
        <p  ><b>Председатель квалиф  <br> комиссии _______________________________</b>  </p>
        <p  ><b>Инспектор  _______________________________</b>  </p>
        <p  ><b>Директор учебного <br> заведения   ________________________________ </b>    </p>
        <div class='top'>
            <img width='90' height='120' src='{{asset('images/qrcodes/'.$student->qr_name)}}'/>
        </div>

    </div>
</main>
<br>
<main class='main'>
    <div class='row1'>
        <p class='font2'><strong class='font2'>К удостоверению № ____</strong></p>
        <p class='font2'>Гр ________________ подвергнут Периодической проверке знаний в объёме производственной инструкции.</p>
        <p><strong class='font2'>Протокол № __________</strong></p>
        <p><strong class='font2'>По '__'&nbsp; _________________</strong></p>
        <p><strong class='font2'>До '__'&nbsp; _________________</strong></p>
        <p><strong class='font2'>М.П.<br />Председатель </strong></p>
        <p><strong class='font2'>Квалиф. Комиссии _____________________</strong></p>
        <p><strong class='font2'>Инспектор ______________________________</strong></p>
    </div>
    <div class='row1'>
        <p class='font2'><strong class='font2'>К удостоверению № ____</strong></p>
        <p class='font2'>Гр ________________ подвергнут Периодической проверке знаний в объёме производственной инструкции.</p>
        <p><strong class='font2'>Протокол № __________</strong></p>
        <p><strong class='font2'>По '__'&nbsp; _________________</strong></p>
        <p><strong class='font2'>До '__'&nbsp; _________________</strong></p>
        <p><strong class='font2'>М.П.<br />Председатель </strong></p>
        <p><strong class='font2'>Квалиф. Комиссии _____________________</strong></p>
        <p><strong class='font2'>Инспектор ______________________________</strong></p>
    </div>
</main>
<div style='page-break-before: always; clear:both'></div>
<main class='main'>
    <div class='row1' style='margin-left: 25px;'>
        <p class='font2'><strong class='font2'>К удостоверению № ____</strong></p>
        <p class='font2'>Гр ________________ подвергнут Периодической проверке знаний в объёме производственной инструкции.</p>
        <p><strong class='font2'>Протокол № __________</strong></p>
        <p><strong class='font2'>По '__'&nbsp; _________________</strong></p>
        <p><strong class='font2'>До '__'&nbsp; _________________</strong></p>
        <p><strong class='font2'>М.П.<br />Председатель </strong></p>
        <p><strong class='font2'>Квалиф. Комиссии _____________________</strong></p>
        <p><strong class='font2'>Инспектор ______________________________</strong></p>
    </div>
    <div class='row1'></div>
</main>
<br>
<main class='main'>
    <div class='row1' style='margin-left: 25px;'></div>
    <div class='row1' >
        <p class='font2'><strong class='font2'>К удостоверению № ____</strong></p>
        <p class='font2'>Гр ________________ подвергнут Периодической проверке знаний в объёме производственной инструкции.</p>
        <p><strong class='font2'>Протокол № __________</strong></p>
        <p><strong class='font2'>По '__'&nbsp; _________________</strong></p>
        <p><strong class='font2'>До '__'&nbsp; _________________</strong></p>
        <p><strong class='font2'>М.П.<br />Председатель </strong></p>
        <p><strong class='font2'>Квалиф. Комиссии _____________________</strong></p>
        <p><strong class='font2'>Инспектор ______________________________</strong></p>
    </div>
</main>
</textarea>
<script src="https://cdn.tiny.cloud/1/yhkqwyog2j2wp3tbr92cd19v1i7o4x4na65vcbndlhf5tjl9/tinymce/7/tinymce.min.js" referrerpolicy="origin"></script>

<script>
    tinymce.init({
        selector: '#cer',
        height: 800,
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste pagebreak"
        ],
        toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image pagebreak',
        toolbar_mode: 'floating',
        tinycomments_mode: 'embedded',
        tinycomments_author: 'Author name',
        pagebreak_split_block: true,
        content_css: ["{{ asset('styles/assets/guvohnoma/style_6_tur.css') }}"]
    });
</script>

</body>
</html>

