<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Document</title>
</head>
<body>
<textarea id="cer">
<table style='height: 232.2px; width: 376px; margin-top: 0px;' width='3.7px'>
    <tbody>
    <tr style='height: 80px;' valign='top'>
        <td style='border-top: 4.5pt double #000000; border-bottom-style: none; border-left: 4.5pt double #000000; border-right-style: none; padding: 0in 0in 0in 0.08in; height: 80px; width: 82.6792px;' width='96' height='79'>
            <p><img src='{{asset('admin/assets/images/medical/med.png')}}' alt='' width='75' height='67' /></p>
        </td>
        <td style='border-top: 4.5pt double #000000; border-bottom-style: none; border-left-style: none; border-right-style: none; padding: 0in; height: 80px; width: 173.28px;' width='190'>
            <p style='margin-bottom: 0in; text-align: center;'><span style='font-family: sans-serif; font-size: 6pt;'><strong style='font-size: 12px;'>&laquo;SANOAT TEXNIKA UNIVERSAL&raquo;</strong></span></p>
            <p style='margin-bottom: 0in; text-align: center;'><span style='font-family: sans-serif;'><span style='font-size: 10px;'>NODAVLAT TA`LIM MUASSASASI</span></span></p>
            <p style='text-align: center;'><span style='color: #0070c0;'><span style='font-family: sans-serif;'><span style='font-size: 12px;'><strong>GUVOXNOMA</strong></span></span></span><span style='color: #ff0000;'><span style='font-family: sans-serif;'><span style='font-size: 8pt;'><strong></strong></span></span></span><span style='color: #ff0000;'><span style='font-family: sans-serif;'><span style='text-decoration-line: underline;'><strong> №   {{sprintf("%04d", $student->number)}}  </strong></span></span></span></p>
        </td>
        <td style='border-top: 4.5pt double #000000; border-bottom-style: none; border-left-style: none; border-right: 6pt double #000000; padding: 0in 0.08in 0in 0in; height: 80px; width: 90.6917px;' width='116'>
            <p><img width='83' height='56' src='{{asset('admin/assets/images/medical/vodit.jpg')}}'/></p>
        </td>
    </tr>
    <tr style='height: 152.2px;' valign='top'>
        <td style='border-style: none double double; border-bottom-width: 4.5pt; border-bottom-color: #000000; border-left-width: 4.5pt; border-left-color: #000000; border-right-width: 6pt; border-right-color: #000000; padding: 0in 0.08in; height: 152.2px; width: 346.651px;' colspan='3' width='450' height='170'>
            <p style='margin-bottom: 0in; text-align: center;'><span style='font-family: sans-serif;'><span style='text-decoration-line: underline;'><strong style='font-size: 12px;'>'{{$student->group_name->organization}}'</strong></span></span><span style='font-family: sans-serif;'><span style='text-decoration-line: underline;'><strong> </strong></span></span><span style='font-family: sans-serif;'><span style='font-size: 6pt;'> </span></span></p>
            <p style='margin-bottom: 0in; text-align: center;'><span style='font-family: sans-serif;'><span style='font-size: 6pt;'>Berildi </span></span><span style='font-family: sans-serif;'><span style='font-size: 6pt; line-height: 0px;'><em><span style='text-decoration-line: underline; font-size: 5pt;'><strong style='font-size: 12px;'> {{$student->student_fio}} </strong></span></em></span></span></p>
            <p style='margin-bottom: 0in; text-align: justify;'><span style='font-family: sans-serif;'><span style='font-size: 6pt;'><strong style='font-size: 12px;'>&laquo;{{$student->malaka}}&raquo;</strong></span></span><span style='font-family: sans-serif;'><span style='font-size: 10px;'> ko`rsatish bo`yicha ({{$student->group_name->kurslar->nazariy_soat + $student->group_name->kurslar->amaliy_soat}}) soatlik dastur asosida o`qitildi.</span></span></p>
            <p style='margin-bottom: 0in; text-align: justify;'><span style='font-family: sans-serif;'><span style='font-size: 10px;'>&nbsp; &nbsp; Asos:{{$student->group_name->group_title}}- sonli bayonnoma </span></span><span style='font-family: sans-serif;'><em><span style='text-decoration-line: underline;'><strong style='font-size: 12px;'> {{$student->group_name->end_date}} y.<code></code></strong></span></em></span><span style='font-family: sans-serif;'>&nbsp; ___________</span></p>
        </td>
    </tr>
    </tbody>
</table>
<div style='break-before: page; clear: both; line-height: 1;'>&nbsp;</div>
<table style='height: 227px; width: 376px; margin-left: 370px; margin-top: -9px;' width='3.7px'>
    <tbody>
    <tr style='height: 100.8px;' valign='top'>
        <td style='border-top: 4.5pt double #000000; border-bottom-style: none; border-left: 4.5pt double #000000; border-right-style: none; padding: 0in 0in 0in 0.08in; height: 100.8px; width: 82.6792px;' width='96' height='79'>
            <p><img src='{{asset('admin/assets/images/medical/med.png')}}' alt='' width='75' height='67' /></p>
        </td>
        <td style='border-top: 4.5pt double #000000; border-bottom-style: none; border-left-style: none; border-right-style: none; padding: 0in; height: 100.8px; width: 173.28px;' width='190'>
            <p style='margin-bottom: 0in; text-align: center;'><span style='font-family: sans-serif; font-size: 6pt;'><strong style='font-size: 10px;'>&laquo;САНОАТ ТЕХНИКА УНИВЕРСАЛ&raquo;</strong></span></p>
            <p style='margin-bottom: 0in; text-align: center;'><span style='font-family: sans-serif;'><span style='font-size: 8px;'>НЕГОСУДАРСТВЕННОЕ ОБРАЗОВАТЕЛЬНОЕ УЧРЕЖДЕНИЕ</span></span></p>
            <p style='text-align: center;'><span style='color: #0070c0;'><span style='font-family: sans-serif;'><span style='font-size: 10px;'><strong style='font-size: 16px;'>УДОСТОВЕРЕНИЕ</strong></span></span></span><span style='color: #ff0000;'><span style='font-family: sans-serif;'><span style='text-decoration-line: underline;'><strong style='font-size: 16px;'>&nbsp; №   {{sprintf("%04d", $student->number)}}  </strong></span></span></span></p>
        </td>
        <td style='border-top: 4.5pt double #000000; border-bottom-style: none; border-left-style: none; border-right: 6pt double #000000; padding: 0in 0.08in 0in 0in; height: 100.8px; width: 90.6917px;' width='116'>
            <p><img width='83' height='56' src='{{asset('images/qrcodes/'.$student->qr_name)}}'/></p>
        </td>
    </tr>
    <tr style='height: 126.2px;' valign='top'>
        <td style='border-style: none double double; border-bottom-width: 4.5pt; border-bottom-color: #000000; border-left-width: 4.5pt; border-left-color: #000000; border-right-width: 6pt; border-right-color: #000000; padding: 0in 0.08in; height: 126.2px; width: 346.651px;' colspan='3' width='450' height='170'>
            <p style='margin-bottom: 0in; text-align: center;'><span style='font-family: sans-serif;'><span style='text-decoration-line: underline;'><strong style='font-size: 12px;'>'{{$student->group_name->organization}}'</strong></span></span><span style='font-family: sans-serif;'><span style='text-decoration-line: underline;'><strong> </strong></span></span><span style='font-family: sans-serif;'><span style='font-size: 6pt;'> </span></span></p>
            <p style='margin-bottom: 0in; text-align: center;'><span style='font-family: sans-serif;'><span style='font-size: 6pt;'>Выдано </span></span><span style='font-family: sans-serif;'><span style='font-size: 6pt; line-height: 0px;'><em><span style='text-decoration-line: underline; font-size: 5pt;'><strong style='font-size: 12px;'> {{$student->student_fio}} </strong></span></em></span></span></p>
            <p style='margin-bottom: 0in; text-align: justify;'><span style='font-family: sans-serif;'><span style='font-size: 6pt;'><strong style='font-size: 12px;'>В том, что он (она) прошел(а) курс обучение по программе &laquo;{{$student->malaka}}&raquo; в объеме {{$student->group_name->kurslar->nazariy_soat + $student->group_name->kurslar->amaliy_soat}} часов.</strong></span></span></p>
            <p style='margin-bottom: 0in; text-align: justify;'><span style='font-family: sans-serif;'><span style='font-size: 10px;'>&nbsp; &nbsp; Основание Протокол </span></span><span style='font-family: sans-serif;'><em><span style='text-decoration-line: underline;'><strong style='font-size: 12px;'>{{$student->group_name->end_date}} y.<code></code></strong></span></em></span><span style='font-family: sans-serif;'>&nbsp; ___________</span></p>
        </td>
    </tr>
    </tbody>
</table>

</textarea>
<script src="https://cdn.tiny.cloud/1/yhkqwyog2j2wp3tbr92cd19v1i7o4x4na65vcbndlhf5tjl9/tinymce/7/tinymce.min.js" referrerpolicy="origin"></script>

<script>
    tinymce.init({
        selector: '#cer',
        height: 800,
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste pagebreak"
        ],
        toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image pagebreak',
        toolbar_mode: 'floating',
        tinycomments_mode: 'embedded',
        tinycomments_author: 'Author name',
        pagebreak_split_block: true,
        content_css: ["{{ asset('styles/assets/guvohnoma/style_12_tur.css') }}"]
    });
</script>

</body>
</html>
