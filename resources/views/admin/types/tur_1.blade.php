<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">

    <title>Document</title>
</head>
<body>
<textarea id="cer">

<main class='main'>
<div class='row'>
<p class='title' > <b style='font-size: 1.2em;'>СВИДЕТЕЛЬСТВО </b> <b class='ml'  style='color: red; font-size: 1.2em;'> №{{sprintf("%04d", $student->number)}}</b></p>
<div class='row-2' style='font-size: 1rem;' >
<div class='img'></div>
<p class='font'> Выдано: <b class='font1'>  {{$student->student_fio}} </b>
<br> в том, что он (а) с  {{$student->group_name->start_date}}  года по  {{$student->group_name->end_date}}  года обучался (лось) по профессии</p>
</div>
<p class='font'  ><b class='font1'>{{$student->malaka}} </b>  </p>
<p class='center ' style='margin: 0px'> <span style='font-size:12px ;'> (наименование профессии) </span> </p>
<p class='font' >  {{$student->group_name->kurslar->showStudyPlan->plan_title}} </p>
<p class='center' style='margin: 0px'> <span style='font-size:12px;'> (форма обучения)</span> </p>
<p class='font' > <b class='font1'> НОУ«Саноат техника универсал Сервис» </b> г.Бухара </p>
<p class='center' style='margin: 0px'> <span style='font-size:12px;'> (наименование предприятия организации) </span> </p>
<p class='last'>Прошел(а) полный курс теоретического обучения в объеме {{$student->group_name->kurslar->nazariy_soat}} часов и производственного обучения в объеме  {{$student->group_name->kurslar->amaliy_soat}} часов и сдал(а) квалификационный экзамен с оценками:<p>
<p><b style='font-size: 1.2em;'>- теория:  {{$student->nazariy_baho}} </b> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b style='font-size: 1.2em;'>- практика: {{$student->amaliy_baho}}      </b>   </p>
<p>  </p>

</div>
<div class='row' style='margin-left:26px'>
<p class='font'  > Решением (Экзаменационной)<br> Квалификационной комиссии</p>
<p  class='font' > от {{$student->group_name->end_date}}</b>  г  Протокол №{{$student->group_name->group_title}}  </p>
<p  class='center title-2' > <b class='font1'>гр  {{$student->student_fio}}</b> </p> <br>
<p class='last'> Установлен тарифно-квалификационный <br> разряд (класс категория) {{$student->razryad}}   <br>  по профессии:</p>
<p class='font1' ><b class='font'>{{$student->malaka}}</b>  </p> <br>
<p class='last1' >Председатель  квалификационной комиссии:______________________________  </p>
<br>
<p class='last' >Руководитель  предприятия <br> Организации:__________________________  <br>           </p>
<br> <br><br>
<p class='last' > <b style='font-size: 0.9em;'>М.П.<br> НОУ«Саноат  техника  универсал <br> сервис»    <b> </p>
    <div class='top'>
            <img width='90' height='120' src='{{asset('images/qrcodes/'.$student->qr_name)}}'/>
        </div>
</div>
</main>

</textarea>
<script src="https://cdn.tiny.cloud/1/yhkqwyog2j2wp3tbr92cd19v1i7o4x4na65vcbndlhf5tjl9/tinymce/7/tinymce.min.js" referrerpolicy="origin"></script>

<script>
    tinymce.init({
        selector: '#cer',
        height: 800,
        plugins: [
            "advlist autolink lists link image charmap print preview anchor",
            "searchreplace visualblocks code fullscreen",
            "insertdatetime media table contextmenu paste pagebreak"
        ],
        toolbar: 'insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image pagebreak',
        toolbar_mode: 'floating',
        tinycomments_mode: 'embedded',
        tinycomments_author: 'Author name',
        pagebreak_split_block: true,
        content_css: ["{{ asset('styles/assets/guvohnoma/style_1_tur.css') }}"]
    });
</script>

</body>
</html>
