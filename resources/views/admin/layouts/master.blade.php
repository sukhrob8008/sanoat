<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Cuba admin is super flexible, powerful, clean &amp; modern responsive bootstrap 5 admin template with unlimited possibilities.">
    <meta name="keywords" content="admin template, Cuba admin template, dashboard template, flat admin template, responsive admin template, web app">
    <meta name="author" content="pixelstrap">
    <link rel="icon" href="https://style.softgo.uz/assets/images/favicon.png" type="image/x-icon">
    <link rel="shortcut icon" href="https://style.softgo.uz/assets/images/favicon.png" type="image/x-icon">
    <title>@yield('title')</title>
    <!-- Google font-->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.2.1/css/all.min.css" integrity="sha512-MV7K8+y+gLIBoVD59lQIYicR65iaqukzvf/nwasF0nqhPay5w/9lJmVM2hMDcnK1OnMGCdVK+iQrJ7lzPJQd1w==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link href="https://fonts.googleapis.com/css?family=Rubik:400,400i,500,500i,700,700i&amp;display=swap" rel="stylesheet">
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,300i,400,400i,500,500i,700,700i,900&amp;display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="https://style.softgo.uz/assets/css/font-awesome.css'">
    <!-- ico-font-->
    <link rel="stylesheet" type="text/css" href="https://style.softgo.uz/assets/css/vendors/icofont.css">
    <!-- Themify icon-->
    <link rel="stylesheet" type="text/css" href="https://style.softgo.uz/assets/css/vendors/themify.css">
    <!-- Flag icon-->
    <link rel="stylesheet" type="text/css" href="https://style.softgo.uz/assets/css/vendors/flag-icon.css">
    <!-- Feather icon-->
    <link rel="stylesheet" type="text/css" href="https://style.softgo.uz/assets/css/vendors/feather-icon.css">
    <!-- Plugins css start-->
    <link rel="stylesheet" type="text/css" href="https://style.softgo.uz/assets/css/vendors/echart.css">
    <link rel="stylesheet" type="text/css" href="https://style.softgo.uz/assets/css/vendors/page-builder.css">
    <link rel="stylesheet" type="text/css" href="https://style.softgo.uz/assets/css/vendors/select2.css">
    <link rel="stylesheet" type="text/css" href="https://style.softgo.uz/assets/css/vendors/scrollbar.css">
    <link rel="stylesheet" type="text/css" href="https://style.softgo.uz/assets/css/vendors/datatables.css">
    <link rel="stylesheet" type="text/css" href="https://style.softgo.uz/assets/css/vendors/owlcarousel.css">
    <link rel="stylesheet" type="text/css" href="https://style.softgo.uz/assets/css/vendors/rating.css">
    <link rel="stylesheet" type="text/css" href="https://style.softgo.uz/assets/css/vendors/animate.css">
{{--    <link rel="stylesheet" type="text/css" href="{{asset('admin/assets/css/vendors/chartist.css')}}">--}}
    <link rel="stylesheet" type="text/css" href="https://style.softgo.uz/assets/css/vendors/date-picker.css">
    <!-- Plugins css Ends-->
    <!-- Bootstrap css-->
    <link rel="stylesheet" type="text/css" href="https://style.softgo.uz/assets/css/vendors/bootstrap.css">
    <!-- App css-->
    <link rel="stylesheet" type="text/css" href="https://style.softgo.uz/assets/css/style.css">
    <link id="color" rel="stylesheet" href="https://style.softgo.uz/assets/css/color-1.css" media="screen">
    <!-- Responsive css-->
    <link rel="stylesheet" type="text/css" href="https://style.softgo.uz/assets/css/responsive.css">
    @stack('style')
    {{--    <link--}}
    {{--        rel="stylesheet"--}}
    {{--        href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css"--}}
    {{--    />--}}
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body onload="startTime()">
<div class="loader-wrapper">
    <div class="loader-index"><span></span></div>
    <svg>
        <defs></defs>
        <filter id="goo">
            <fegaussianblur in="SourceGraphic" stddeviation="11" result="blur"></fegaussianblur>
            <fecolormatrix in="blur" values="1 0 0 0 0  0 1 0 0 0  0 0 1 0 0  0 0 0 19 -9" result="goo"> </fecolormatrix>
        </filter>
    </svg>
</div>
<!-- tap on top starts-->
<div class="tap-top"><i data-feather="chevrons-up"></i></div>
<!-- tap on tap ends-->
<!-- page-wrapper Start-->
<div class="page-wrapper compact-wrapper" id="pageWrapper">
    <!-- Page Header Start-->
    @include('admin.partials.navbar')
    <!-- Page Header Ends                              -->
    <!-- Page Body Start-->
    <div class="page-body-wrapper">
        <!-- Page Sidebar Start-->
        @include('admin.partials.sidebar')
        <!-- Page Sidebar Ends-->
        <div class="page-body pt-5">
            @yield('content')
        </div>
        <!-- footer start-->
        <footer class="footer">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-md-12 footer-copyright text-center">
                        <p class="mb-0">Copyright 2021 © Cuba theme by pixelstrap  </p>
                    </div>
                </div>
            </div>
        </footer>
    </div>
</div>
<!-- latest jquery-->
<script src="https://style.softgo.uz/assets/js/jquery-3.5.1.min.js"></script>
<script src="https://style.softgo.uz/assets/js/select2/select2.full.min.js"></script>
<script src="https://style.softgo.uz/assets/js/select2/select2-custom.js"></script>
<!-- Bootstrap js-->
<script src="https://style.softgo.uz/assets/js/bootstrap/bootstrap.bundle.min.js"></script>
<!-- feather icon js-->
<script src="https://style.softgo.uz/assets/js/icons/feather-icon/feather.min.js"></script>
<script src="https://style.softgo.uz/assets/js/icons/feather-icon/feather-icon.js"></script>
<!-- scrollbar js-->
<script src="https://style.softgo.uz/assets/js/scrollbar/simplebar.js"></script>
<script src="https://style.softgo.uz/assets/js/scrollbar/custom.js"></script>
<!-- Sidebar jquery-->
<script src="https://style.softgo.uz/assets/js/config.js"></script>
<!-- Plugins JS start-->
<script src="https://style.softgo.uz/assets/js/sidebar-menu.js"></script>
<script src="https://style.softgo.uz/assets/js/chart/chartist/chartist.js"></script>
<script src="https://style.softgo.uz/assets/js/chart/chartist/chartist-plugin-tooltip.js"></script>
<script src="https://style.softgo.uz/assets/js/chart/knob/knob.min.js"></script>
<script src="https://style.softgo.uz/assets/js/chart/knob/knob-chart.js"></script>
<script src="https://style.softgo.uz/assets/js/chart/apex-chart/apex-chart.js"></script>
<script src="https://style.softgo.uz/assets/js/chart/apex-chart/stock-prices.js"></script>
<script src="https://style.softgo.uz/assets/js/notify/bootstrap-notify.min.js"></script>
<script src="https://style.softgo.uz/assets/js/dashboard/default.js"></script>
<script src="https://style.softgo.uz/assets/js/notify/index.js"></script>
<script src="https://style.softgo.uz/assets/js/datepicker/date-picker/datepicker.js"></script>
<script src="https://style.softgo.uz/assets/js/datepicker/date-picker/datepicker.en.js"></script>
<script src="https://style.softgo.uz/assets/js/datepicker/date-picker/datepicker.custom.js"></script>
<script src="https://style.softgo.uz/assets/js/typeahead/handlebars.js"></script>
<script src="https://style.softgo.uz/assets/js/typeahead/typeahead.bundle.js"></script>
<script src="https://style.softgo.uz/assets/js/typeahead/typeahead.custom.js"></script>
<script src="https://style.softgo.uz/assets/js/typeahead-search/handlebars.js"></script>
<script src="https://style.softgo.uz/assets/js/typeahead-search/typeahead-custom.js"></script>
<script src="https://style.softgo.uz/assets/js/datatable/datatables/jquery.dataTables.min.js"></script>
<script src="https://style.softgo.uz/assets/js/rating/jquery.barrating.js"></script>
<script src="https://style.softgo.uz/assets/js/rating/rating-script.js"></script>
<script src="https://style.softgo.uz/assets/js/owlcarousel/owl.carousel.js"></script>
<script src="https://style.softgo.uz/assets/js/ecommerce.js"></script>
<script src="https://style.softgo.uz/assets/js/product-list-custom.js"></script>
<script src="https://style.softgo.uz/assets/js/tooltip-init.js"></script>
{{--<script src="{{asset('admin/assets/js/chart/echart/esl.js')}}"></script>--}}
{{--<script src="{{asset('admin/assets/js/chart/echart/config.js')}}"></script>--}}
{{--<script src="{{asset('admin/assets/js/chart/echart/pie-chart/facePrint.js')}}"></script>--}}
{{--<script src="{{asset('admin/assets/js/chart/echart/pie-chart/testHelper.js')}}"></script>--}}
{{--<script src="{{asset('admin/assets/js/chart/echart/pie-chart/custom-transition-texture.js')}}"></script>--}}
{{--<script src="{{asset('admin/assets/js/chart/echart/data/symbols.js')}}"></script>--}}
{{--<script src="{{asset('admin/assets/js/chart/echart/custom.js')}}"></script>--}}
{{--<script src="{{asset('admin/assets/js/editor/ckeditor/ckeditor.js')}}"></script>--}}
{{--<script src="{{asset('admin/assets/js/editor/ckeditor/adapters/jquery.js')}}"></script>--}}
{{--<script src="{{asset('admin/assets/js/jquery.ui.min.js')}}"></script>--}}
{{--<script src="{{asset('admin/assets/js/page-builder/jquery.grideditor.min.js')}}"></script>--}}
{{--<script src="{{asset('admin/assets/js/page-builder/page-builder-custom.js')}}"></script>--}}
<!-- Plugins JS Ends-->
<!-- Theme js-->
<script src="https://style.softgo.uz/assets/js/script.js"></script>
{{--<script src="{{asset('admin/assets/js/theme-customizer/customizer.js')}}"></script>--}}
<script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/2.1.0/sweetalert.min.js"></script>
@include('sweetalert::alert')
@stack('script')
</body>
</html>
