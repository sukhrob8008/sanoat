@extends('admin.layouts.master')
@section('title', 'Гувохномалар рўйхати')
@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>СТ Универсалда гувохномалар рўйхати</h5>
                        <span>Ushbu jadvaldan siz ta'lim muassasidagi o'quv rejalarini ko'rishingiz, tahrirlash ishlarini, shuningdek o'quv rejani bazadan o'chirib tashlashingiz mumkin.</span>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive product-table">
                            <table class="display" id="basic-1">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th class="col-sm-3">Гувохномалар номи</th>
                                    <th class="col-sm-3">Гувохномалар яратилган вакти</th>
                                    <th>Тахрирлаш</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($guvohnomas as $guvohnoma)
                                    <tr>
                                        <td>{{$loop->index+1}}</td>
                                        <td class="col-sm-3">
                                            {{$guvohnoma->tur_title}}
                                        </td>
                                        <td class="col-sm-3">
                                            {{ date('d-m-Y', strtotime($guvohnoma->created_at))}}
                                        </td>
                                        <td>
                                            <a class="btn btn-primary" href="{{route('guvohnoma.edit', $guvohnoma)}}" style="padding: 6px 10px ;"><i class="far fa-edit"></i></a>
                                            <form class="d-inline-block" method="post" action="{{route('guvohnoma.destroy', $guvohnoma)}}">
                                                @csrf
                                                @method('DELETE')
                                                <button type="submit" class="btn btn-danger show_confirm" data-toggle="tooltip" title='Delete' style="padding: 6px 10px ;"><i class="far fa-trash-alt"></i></button>
                                            </form>

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('script')
    <script type="text/javascript">

        $('.show_confirm').click(function(event) {
            var form =  $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                title: `Ушбу гувохномани ўчирмоқчимисиз?`,
                // text: "If you delete this, it will be gone forever.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });

    </script>
@endpush
