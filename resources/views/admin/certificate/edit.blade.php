@extends('admin.layouts.master')
@section('title', 'Гувохнома таҳрирлаш')
@section('content')
    <div class="card">
        <div class="card-body">
            <form class="" action="{{route('guvohnoma.update', $guvohnoma)}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="mb-3">
                    <label for="tur_title">Гувохнома номи</label>
                    <input value="{{$guvohnoma->tur_title}}" name="tur_title" class="form-control @error('tur_title') is-invalid @enderror"
                           type="text" placeholder="Гувохнома номи">
                    @error('tur_title')
                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                    @enderror
                </div>
                <div class="mb-3">
                    <button class="btn btn-primary" type="submit">Гувохнома яратиш</button>
                </div>
            </form>
        </div>
    </div>
@endsection
