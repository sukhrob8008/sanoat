@extends('admin.layouts.master')
@section('title', 'Тингловчи маълумотларини таҳрирлаш')
@section('content')
    <div class="card">

        <div class="card-body">
            <form class="" action="{{route('student.update', $student)}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="mb-3">
                    <label for="student_fio">Тингловчи Ф.И.О</label>
                    <input value="{{$student->student_fio}}" name="student_fio" class="form-control @error('student_fio') is-invalid @enderror"
                           type="text" placeholder="Тингловчи Ф.И.О">
                    @error('student_fio')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                </div>
                <div class="mb-3">
                    <div class="col-form-label">Гуруҳни танланг</div>
                    <select name="group_id" class="js-example-basic-single col-sm-12 @error('group_id') is-invalid @enderror">
                        @foreach($groups as $group)
                            @if($group->id == $student->group_id)
                                <option selected value="{{$group->id}}">{{$group->group_title." ( ".$group->kurslar->course_title."(".$group->kurslar->showStudyPlan->plan_title."))"}}</option>
                            @else
                                <option value="{{$group->id}}">{{$group->group_title." ( ".$group->kurslar->course_title."(".$group->kurslar->showStudyPlan->plan_title."))"}}</option>
                            @endif
                        @endforeach
                    </select>
                    @error('group_id')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="malaka">Курсни киритинг</label>
                    <input value="{{$student->malaka}}" name="malaka" class="form-control @error('malaka') is-invalid @enderror"
                           type="text" placeholder="Курсни киритинг">
                    @error('malaka')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                </div>

                <div class="mb-3">
                    <div class="col-form-label">Турни танланг</div>
                    <select name="guvohnoma_id" class="js-example-basic-single col-sm-12 @error('guvohnoma_id') is-invalid @enderror">

                        @foreach($guvohnomas as $guvohnoma)
                            @if($student->guvohnoma_id == $guvohnoma->id)
                                <option selected value="{{$guvohnoma->id}}">{{$guvohnoma->tur_title}}</option>
                            @else
                                <option value="{{$guvohnoma->id}}">{{$guvohnoma->tur_title}}</option>
                            @endif
                        @endforeach
                    </select>
                    @error('group_id')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="razryad">Разрядни киритинг</label>
                    <input value="{{$student->razryad}}" name="razryad" class="form-control" type="text" placeholder="Разрядни киритинг">
                </div>
                <div class="mb-3">
                    <label for="pasport_seria">Пасспорт серия</label>
                    <input maxlength="2" value="{{$student->pasport_seria}}" name="pasport_seria" class="form-control @error('pasport_seria') is-invalid @enderror"
                           type="text" placeholder="Пасспорт серия">
                    @error('pasport_seria')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="pasport_number">Пасспорт рақам</label>
                    <input value="{{$student->pasport_number}}" name="pasport_number" class="form-control @error('pasport_number') is-invalid @enderror"
                           type="number" placeholder="Пасспорт рақам">
                    @error('pasport_number')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="amaliy_baho">Тингловчи амалий баҳоси</label>
                    <input value="{{$student->amaliy_baho}}" name="amaliy_baho" class="form-control @error('amaliy_baho') is-invalid @enderror"
                           type="number" placeholder="Тингловчи амалий баҳоси">
                    @error('amaliy_baho')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="nazariy_baho">Тингловчи назарий баҳоси</label>
                    <input value="{{$student->nazariy_baho}}" name="nazariy_baho" class="form-control @error('nazariy_baho') is-invalid @enderror"
                           type="number" placeholder="Тингловчи назарий баҳоси">
                    @error('nazariy_baho')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="profession">Tinglovchining kasbi</label>
                    <input value="{{$student->profession}}" name="profession" class="form-control @error('profession') is-invalid @enderror"
                           type="text" placeholder="Tinglovchining kasbi">
                    @error('profession')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="student_img">Тингловчининг расми</label>
                    <input name="student_img" class="form-control image @error('student_img') is-invalid @enderror" type="file" id="imageUpload"><br>
                    <input type="hidden" name="x1" value="" />
                    <input type="hidden" name="y1" value="" />
                    <input type="hidden" name="w" value="" />
                    <input type="hidden" name="h" value="" />
                    <p><img id="previewimage" style="display:none;"/></p>
                    @if ($path = \Illuminate\Support\Facades\Session::get('path'))
                        <img width="100%" height="100%" src="{{ $path }}" />
                    @endif
                    @error('student_img')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                </div>
                <div class="mb-3">
                    <button class="btn btn-primary" type="submit">Тингловчи маълумотларини ўзгартириш</button>
                </div>
            </form>


        </div>
    </div>
@endsection
@push('style')
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/imgareaselect/0.9.10/css/imgareaselect-default.css"/>
@endpush
@push('script')
    <script src="https://cdnjs.cloudflare.com/ajax/libs/imgareaselect/0.9.10/js/jquery.imgareaselect.min.js"></script>
    <script>
        jQuery(function($) {
            var p = $("#previewimage");

            $("body").on("change", ".image", function(){
                var imageReader = new FileReader();
                imageReader.readAsDataURL(document.querySelector(".image").files[0]);

                imageReader.onload = function (oFREvent) {
                    p.attr('src', oFREvent.target.result).fadeIn();
                };
            });

            $('#previewimage').imgAreaSelect({
                onSelectEnd: function (img, selection) {
                    $('input[name="x1"]').val(selection.x1);
                    $('input[name="y1"]').val(selection.y1);
                    $('input[name="w"]').val(selection.width);
                    $('input[name="h"]').val(selection.height);
                }
            });
        });
    </script>
@endpush
