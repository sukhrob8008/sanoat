<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
    <link rel="stylesheet" href="https://style.softgo.uz/styles/assets/css/test.css">
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>

    <title>Account page</title>
</head>
<body>
<div class="container emp-profile">
    <form method="post">
        <div class="row">
            <div class="col-md-4">
                <div class="profile-img">
                    @if (file_exists(public_path('images/students/'.$student->student_img)))
                        <img style="border-radius: 5px" src='{{ asset('images/students/'.$student->student_img) }}' alt='user'>
                    @else
                        <img style="border-radius: 5px" src='{{ $student->student_img }}' alt='user'>
                    @endif
                    <div class="file btn btn-lg btn-primary">
                        Фото слушателя
                    </div>
                </div>
            </div>
            <div class="col-md-8">
                <div class="profile-head">
                    <h5>
                        {{$student->student_fio}}
                    </h5>
                    <h6>
                        <span style="color: #e85454">Профессия:</span> {{$student->malaka}}
                    </h6>
                    <p class="proile-rating" style="font-size: 22px; color: #e85454">Регистрационный номер: <span style="font-size: 22px; color: #0565a8">№{{sprintf("%04d", $student->number)}}</span></p>
                    <ul class="nav nav-tabs" id="myTab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">Информация о слушателя</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-4">
                <div class="profile-work">




                </div>
            </div>
            <div class="col-md-8">
                <div class="tab-content profile-tab" id="myTabContent">
                    <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                        <div class="row">
                            <div class="col-md-6">
                                <label>Пасспорт</label>
                            </div>
                            <div class="col-md-6">
                                <p>{{$student->pasport_seria."".$student->pasport_number}}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>Разряд</label>
                            </div>
                            <div class="col-md-6">
                                <p>{{$student->razryad}}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>Изученный курс</label>
                            </div>
                            <div class="col-md-6">
                                <p>{{$student->malaka}}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>Тип образования</label>
                            </div>
                            <div class="col-md-6">
                                <p>{{$student->group_name->kurslar->showStudyPlan->plan_title}}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>Период обучения</label>
                            </div>
                            <div class="col-md-6">
                                <p>{{"с ". $student->group_name->start_date. " года по " . $student->group_name->end_date . " года."}}</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-6">
                                <label>Количество часов обучения</label>
                            </div>
                            <div class="col-md-6">
                                <p>{{$student->group_name->kurslar->amaliy_soat+$student->group_name->kurslar->nazariy_soat. " ч."}}</p>
                            </div>
                        </div>
                        <div class="row" style="margin-left: 3px">
                                <a href="https://drive.google.com/file/d/10HJJ09c34970XPui51544ql298cCR8Mw/view?usp=sharing" class="btn btn-success text-white">Guvohnoma</a>&nbsp;
                                <a href="https://drive.google.com/file/d/1VO9SynDE_yepVOa6vjXFjbX_QZNDQDNg/view?usp=sharing" class="btn btn-primary text-white">Litsenziya</a>
                        </div>
                    </div>

                </div>
            </div>


        </div>
    </form>
</div>
</body>
</html>
{{--https://demos.creative-tim.com/material-bootstrap-wizard/wizard-book-room.html?_ga=2.223376923.1279095388.1679857296-2071359335.1679207123--}}
