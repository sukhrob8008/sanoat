@extends('admin.layouts.master')
@section('title', 'Тингловчилар жадвали')
@section('content')
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>СТУ Сервисда тингловчилар жадвали</h5><br>
{{--                        <span>Ушбу жадвалдан сиз таълим муассасидаги тингловчиларни кўришингиз, таҳрирлаш ишларини, шунингдек тингловчини базадан ўчириб ташлашингиз мумкин.</span>--}}
                        <form class="d-flex align-center justify-content-between" method="get">
                            <input value="{{ \request()->search }}" class="form-control w-75" type="search" name="search" placeholder="F.I.O, guruh, passport, qr-code...">
                            <input type="submit" class="btn btn-sm btn-success" value="Filtr">
                        </form>
                    </div>
                    <div class="card-body">
                        <div class="table table-striped">
                            <table class="display">
                                <thead>
                                <tr>
{{--                                    <th>#</th>--}}
                                    <th class="col-sm-2">Тингловчи Ф.И.О</th>
                                    <th class="col-sm-3">Гуруҳ, Курси</th>
                                    <th class="col-sm-1">Рег. номер</th>
                                    <th class="col-sm-1">Пасспорт</th>
                                    <th class="col-sm-2">QR-code</th>
                                    <th class="col-sm-2">Гувоҳнома</th>
                                    <th class="col-sm-2">Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($students as $student)
                                    @if($student->active == 0)
                                      <tr style="color: #e3042f">
{{--                                        <td>{{ ($students->currentpage()-1)-$students->perpage() + ($loop->index+1)}}</td>--}}
                                        <td class="col-sm-2">
                                            <a target="_blank" href="{{route('show.info', $student)}}">{{$student->student_fio}}</a>
                                        </td>
                                        <td class="col-sm-3">
                                            {{$student->group_name->group_title." ( ".$student->malaka." ) ".$student->group_name->kurslar->showStudyPlan->plan_title}}
                                        </td>
                                        <td class="col-sm-1">
                                            {{$student->number}}
                                        </td>
                                        <td class="col-sm-1">
                                            {{$student->pasport_seria."".$student->pasport_number}}
                                        </td>
                                        <td class="col-sm-2">

                                            {{   QrCode::size(200)
                                                   ->format('svg')
                                                    ->generate(route('show.info',$student), public_path('images/qrcodes/'.$student->qr_name))
                                            }}
                                            <img width="70" height="70" src="{{asset('images/qrcodes/'.$student->qr_name)}}" alt="">
                                        </td>
                                          <td class="col-sm-2">
                                              <a target="_blank" class="btn btn-warning" href="{{route('student.certi', $student)}}"><i class="fa fa-print"></i></a>
                                          </td>
                                        <td class="col-sm-2">
                                            <a class="btn btn-danger" href="{{route('student.noactive', $student)}}"><i class="fa fa-remove"></i></a>
                                        </td>
                                    </tr>
                                    @else
                                        <tr>
{{--                                            <td>{{($students->currentpage()+ 1)-$students->perpage() + ($loop->index+1)}}</td>--}}
                                            <td class="col-sm-2">
                                                <a target="_blank" href="{{route('show.info', $student)}}">{{$student->student_fio}}</a>
                                            </td>
                                            <td class="col-sm-3">
                                                {{$student->group_name->group_title." ( ".$student->malaka." ) ".$student->group_name->kurslar->showStudyPlan->plan_title}}
                                            </td>
                                            <td class="col-sm-2">
                                                {{$student->number}}
                                            </td>
                                            <td class="col-sm-1">
                                                {{$student->pasport_seria."".$student->pasport_number}}
                                            </td>
                                            <td class="col-sm-2">

                                                {{   QrCode::size(200)
                                                       ->format('svg')
                                                        ->generate(route('show.info',$student), public_path('images/qrcodes/'.$student->qr_name))
                                                }}
                                                <img width="70" height="70" src="{{asset('images/qrcodes/'.$student->qr_name)}}" alt="">
                                            </td>
                                            <td class="col-sm-2">
                                                  <a class="btn btn-warning" href="{{route('student.certi', $student)}}"><i class="fa fa-print"></i></a>
                                            </td>
                                            <td class="col-sm-2">
                                                <a target="_blank" class="btn btn-primary" href="{{route('student.edit', $student)}}"><i class="far fa-edit"></i></a>
                                                <a class="btn btn-success" href="{{route('student.active', $student)}}"><i class="fa fa-check"></i></a>
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="card-footer">
                        {{ $students->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
{{--    <script type="text/javascript">--}}

{{--        $('.show_confirm').click(function(event) {--}}
{{--            var form =  $(this).closest("form");--}}
{{--            var name = $(this).data("name");--}}
{{--            event.preventDefault();--}}
{{--            swal({--}}
{{--                title: `Ushbu kursni o'chirmoqchimisiz?`,--}}
{{--                // text: "If you delete this, it will be gone forever.",--}}
{{--                icon: "warning",--}}
{{--                buttons: true,--}}
{{--                dangerMode: true,--}}
{{--            })--}}
{{--                .then((willDelete) => {--}}
{{--                    if (willDelete) {--}}
{{--                        form.submit();--}}
{{--                    }--}}
{{--                });--}}
{{--        });--}}

{{--    </script>--}}
@endsection
@push('style')
{{--    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.21/css/jquery.dataTables.min.css" integrity="sha512-1k7mWiTNoyx2XtmI96o+hdjP8nn0f3Z2N4oF/9ZZRgijyV4omsKOXEnqL1gKQNPy2MTSP9rIEWGcH/CInulptA==" crossorigin="anonymous" referrerpolicy="no-referrer" />--}}
@endpush
@push('script')
{{--    <script src="https://cdnjs.cloudflare.com/ajax/libs/datatables/1.10.21/js/jquery.dataTables.min.js" integrity="sha512-BkpSL20WETFylMrcirBahHfSnY++H2O1W+UnEEO4yNIl+jI2+zowyoGJpbtk6bx97fBXf++WJHSSK2MV4ghPcg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>--}}
{{--    <script>--}}
{{--        $(document).ready(function (){--}}
{{--            console.log('{{ route('getDataJSON') }}')--}}
{{--           $("#basic-1").DataTable({--}}
{{--               processing: true,--}}
{{--               serverSide: true,--}}
{{--               ajax: {--}}
{{--                   type:'GET',--}}
{{--                   url: '{{ route('getDataJSON') }}',--}}
{{--                   dataSrc: 'data'--}}
{{--               },--}}
{{--               buttons: false,--}}
{{--               searching: true,--}}
{{--               scrollY:500,--}}
{{--               scrollX:true,--}}
{{--               scrollCollapse: true,--}}
{{--               columns: [--}}
{{--                   {data: "id"},--}}
{{--                   {data: "student_fio"},--}}
{{--                   {data: "group_id"},--}}
{{--                   {data: "number"},--}}
{{--                   {data: [ "pasport_seria", "pasport_number" ] },--}}
{{--                   {data: "qr_name"},--}}
{{--                   {data: "guvohnoma_id"},--}}
{{--                   {data: "pasport_number"},--}}
{{--               ]--}}
{{--           })--}}
{{--        })--}}
{{--    </script>--}}

@endpush
