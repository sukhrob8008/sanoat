@extends('admin.layouts.master')
@section('title', 'Guruhlar')
@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header d-flex align-items-center justify-content-between">
                        <h5>СТ Универсалда <span style="color: red">{{$course->course_title}}</span> йўналиши таркибидаги гуруҳлар жадвали</h5>
                        <a href="{{ route('createGroupCourse', $course->id) }}" class="btn btn-success">Гуруҳ яратиш</a>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive product-table">
                            <table class="display" id="basic-1">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th class="col-sm-2">Гуруҳ номи</th>
                                    <th class="col-sm-3">Йўналиш номи</th>
                                    <th class="col-sm-2">Бошланиш</th>
                                    <th class="col-sm-2">Тугаш</th>
                                    <th class="col-sm-3">Раислар</th>
                                    <th class="col-sm-3">Ташкилот</th>
                                    <th class="col-sm-2">Ҳолат</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($groups as $group)
                                    <tr>
                                        <td>{{$loop->index+1}}</td>
                                        <td class="col-sm-1">
                                            <a target="_blank" href="{{route('group.itemStudent', $group)}}">{{$group->group_title}}</a>
                                        </td>
                                        <td class="col-sm-4">
                                            {{$group->kurslar->course_title." (".$group->kurslar->showStudyPlan->plan_title.") ".substr($group->end_date,6,9)}}
                                        </td>
                                        <td class="col-sm-2">
                                            {{ $group->start_date }}
                                        </td>
                                        <td class="col-sm-2">
                                            {{$group->end_date}}
                                        </td>
                                        <td class="col-sm-1">
                                            {{$group->teacher_one." ".$group->teacher_two}}
                                        </td>
                                        <td class="col-sm-1">
                                            {{$group->organization}}
                                        </td>
                                        <td class="col-sm-1">
                                            @if($group->active == 1)
                                                <a href="{{route('group.active_org', ['course_id' =>$course->id, 'group_id' => $group->id])}}" class="btn btn-success" style="padding: 5px 10px"><i data-feather="check"></i></a>
                                            @elseif($group->active == 0)
                                                <a href="{{route('group.noactive_org', ['course_id' =>$course->id, 'group_id' => $group->id])}}" class="btn btn-danger" style="padding: 5px 10px"><i data-feather="x-circle"></i></a>
                                            @endif
                                        </td>
                                        <td class="col-sm-1">
                                            <a class="btn btn-primary" href="{{route('group.edit_org', ['course_id' =>$course->id, 'group_id' => $group->id])}}" style="padding: 5px 10px"><i data-feather="edit"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
{{--                            <table class="display" id="basic-1">--}}
{{--                                <thead>--}}
{{--                                <tr>--}}
{{--                                    <th class="col-sm-1">#</th>--}}
{{--                                    <th class="col-sm-1">Guruh nomi</th>--}}
{{--                                    <th class="col-sm-3">O'quv reja</th>--}}
{{--                                    <th class="col-sm-2">Boshlanish</th>--}}
{{--                                    <th class="col-sm-2">Tugash</th>--}}
{{--                                    <th class="col-sm-3">Guruh raislar</th>--}}
{{--                                    <th class="col-sm-3">Tashkilot</th>--}}
{{--                                    <th class="col-sm-2">Holat</th>--}}

{{--                                </tr>--}}
{{--                                </thead>--}}
{{--                                <tbody>--}}
{{--                                @foreach($groups as $group)--}}
{{--                                    <tr>--}}
{{--                                        <td class="col-sm-1">{{$loop->index+1}}</td>--}}
{{--                                        <td class="col-sm-2">--}}
{{--                                            <a href="{{route('group.itemStudent', $group)}}">{{$group->group_title}}</a>--}}
{{--                                        </td>--}}
{{--                                        <td class="col-sm-2">--}}
{{--                                            {{$group->kurslar->showStudyPlan->plan_title}}--}}
{{--                                        </td>--}}
{{--                                        <td class="col-sm-2">--}}
{{--                                            {{$group->start_date}}--}}
{{--                                        </td>--}}
{{--                                        <td class="col-sm-1">--}}
{{--                                            {{$group->end_date}}--}}
{{--                                        </td>--}}
{{--                                        <td class="col-sm-1">--}}
{{--                                            {{$group->teacher_one." ".$group->teacher_two}}--}}
{{--                                        </td>--}}
{{--                                        <td class="col-sm-1">--}}
{{--                                            {{$group->organization}}--}}
{{--                                        </td>--}}
{{--                                        <td class="col-sm-1">--}}
{{--                                            @if($group->active == 1)--}}
{{--                                                <a href="{{route('group.active', $group)}}" class="btn btn-success">Enable</a>--}}
{{--                                            @elseif($group->active == 0)--}}
{{--                                                <a href="" class="btn btn-danger">Disable</a>--}}
{{--                                            @endif--}}
{{--                                        </td>--}}

{{--                                    </tr>--}}
{{--                                @endforeach--}}
{{--                                </tbody>--}}
{{--                            </table>--}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

