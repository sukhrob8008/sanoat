@extends('admin.layouts.master')
@section('title', 'Гуруҳлар')
@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>СТУ Сервисда гуруҳлар жадвали</h5>
                        <span>Ушбу жадвалдан сиз таълим муассасидаги гуруҳларни кўришингиз, таҳрирлаш ишларини, шунингдек гуруҳни базадан ўчириб ташлашингиз мумкин.</span>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive product-table">
                            <table class="display" id="basic-1">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th class="col-sm-2">Гуруҳ номи</th>
                                    <th class="col-sm-3">Йўналиш номи</th>
                                    <th class="col-sm-2">Бошланиш</th>
                                    <th class="col-sm-2">Тугаш</th>
                                    <th class="col-sm-3">Раислар</th>
                                    <th class="col-sm-3">Ташкилот</th>
                                    <th class="col-sm-2">Ҳолат</th>
                                    <th>Action</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($groups as $group)
                                    <tr>
                                        <td>{{$loop->index+1}}</td>
                                        <td class="col-sm-1">
                                            <a target="_blank" href="{{route('group.itemStudent', $group)}}">{{$group->group_title}}</a>
                                        </td>
                                        <td class="col-sm-3">
                                            {{$group->kurslar->course_title." (".$group->kurslar->showStudyPlan->plan_title.") ".substr($group->end_date,6,9)}}
                                        </td>
                                        <td class="col-sm-2">
                                            {{$group->start_date}}
                                        </td>
                                        <td class="col-sm-2">
                                            {{$group->end_date}}
                                        </td>
                                        <td class="col-sm-1">
                                            {{$group->teacher_one." ".$group->teacher_two}}
                                        </td>
                                        <td class="col-sm-1">
                                            {{$group->organization}}
                                        </td>
                                        <td class="col-sm-1">
                                            @if($group->active == 1)
                                                <a href="{{route('group.active', $group)}}" class="btn btn-success" style="padding: 5px 10px"><i data-feather="check"></i></a>
                                            @elseif($group->active == 0)
                                                <a href="{{route('group.noactive', $group)}}" class="btn btn-danger" style="padding: 5px 10px"><i data-feather="x-circle"></i></a>
                                            @endif
                                        </td>
                                        <td class="col-sm-3">
                                            <a class="btn btn-primary" href="{{route('group.edit', $group)}}" style="padding: 10px 15px;"><i class="far fa-edit"></i></a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('script')
    <script type="text/javascript">

        $('.show_confirm').click(function(event) {
            var form =  $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                title: `Ushbu kursni o'chirmoqchimisiz?`,
                // text: "If you delete this, it will be gone forever.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });

    </script>
@endpush
