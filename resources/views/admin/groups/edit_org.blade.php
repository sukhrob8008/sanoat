@extends('admin.layouts.master')
@section('title', $group->group_title.'-гуруҳ таҳрирлаш')
@section('content')
    <div class="card">
        <div class="card-body">
            <form class="" action="{{ route('group.update_org', ['course_id' => $course->id, 'group_id' => $group->id]) }}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')

                <div class="mb-3">
                    <label for="group_title">Гуруҳ номи</label>
                    <input value="{{$group->group_title}}" name="group_title" class="form-control @error('group_title') is-invalid @enderror"
                           type="text" placeholder="Гуруҳ номи">
                    @error('group_title')
                    <span class="invalid-feedback" role="alert">
                          <strong>{{ $message }}</strong>
                    </span>
                    @enderror
                </div>

                <div class="mb-3">
                    <div class="col-form-label">Гуруҳ йўналишини танланг</div>
                    <select name="course_id" class="js-example-basic-single col-sm-12 @error('course_id') is-invalid @enderror">
                        <option disabled selected>Гуруҳ йўналишини танланг</option>
                        @foreach($courses as $course)
                            @if($course->id == $group->course_id)
                                <option selected value="{{$course->id}}">{{$course->course_title." (".$course->showStudyPlan->plan_title.")"}}</option>
                            @else
                                <option value="{{$course->id}}">{{$course->course_title." (".$course->showStudyPlan->plan_title.")"}}</option>
                            @endif
                        @endforeach
                    </select>
                    @error('course_id')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="start_date">Гуруҳда ўқиш бошланиш вақти</label>
                    <input value="{{$start_date}}" name="start_date" class="form-control @error('start_date') is-invalid @enderror"
                           type="date">
                    @error('start_date')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="end_date">Гуруҳда ўқиш тугаш вақти</label>
                    <input value="{{$end_date}}" name="end_date" class="form-control @error('end_date') is-invalid @enderror"
                           type="date">
                    @error('end_date')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="teacher_one">Ўқув дарси раиси</label>
                    <input value="{{$group->teacher_one}}" name="teacher_one" class="form-control @error('teacher_one') is-invalid @enderror"
                           type="text" placeholder="Ўқув дарси раиси 1">
                    @error('teacher_one')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="teacher_two">Ўқув дарси раиси</label>
                    <input value="{{$group->teacher_two}}" name="teacher_two" class="form-control @error('teacher_two') is-invalid @enderror"
                           type="text" placeholder="Ўқув дарси раиси 2">
                    @error('teacher_two')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                </div>

                <div class="mb-3">
                    <label for="organization">Гуруҳ қайси ташкилотни буюртмаси</label>
                    <input value="{{$group->organization}}" name="organization" class="form-control @error('organization') is-invalid @enderror"
                           type="text" placeholder="Ташкилот номи">
                    @error('organization')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                </div>
                <div class="mb-3">
                    <button class="btn btn-primary" type="submit">Гуруҳни ўзгартириш</button>
                </div>
            </form>
        </div>
    </div>
@endsection
