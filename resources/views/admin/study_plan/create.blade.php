@extends('admin.layouts.master')
@section('title', 'Ўқув режа яратиш')
@section('content')
    <div class="card">
        <div class="card-body">
            <form class="" action="{{route('direction.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('POST')
                <div class="mb-3">
                    <label for="plan_title">Ўқув режа номи</label>
                    <input name="plan_title" class="form-control @error('plan_title') is-invalid @enderror"
                           type="text" placeholder="Ўқув режа номи">
                    @error('plan_title')
                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                    @enderror
                </div>
                <div class="mb-3">
                    <button class="btn btn-primary" type="submit">Ўқув режа яратиш</button>
                </div>
            </form>
        </div>
    </div>
@endsection
