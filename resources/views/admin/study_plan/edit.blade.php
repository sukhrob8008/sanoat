@extends('admin.layouts.master')
@section('title', 'Ўқув режани ўзгартириш')
@section('content')
    <div class="card">
        <div class="card-body">
            <form class="" action="{{route('direction.update', $direction)}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                <div class="mb-3">
                    <label for="plan_title">Ўқув режа номи</label>
                    <input name="plan_title" class="form-control @error('plan_title') is-invalid @enderror"
                           type="text" value="{{$direction->plan_title}}">
                    @error('plan_title')
                    <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                    @enderror
                </div>
                <div class="mb-3">
                    <button class="btn btn-primary" type="submit">Ўқув режани ўзгартириш</button>
                </div>
            </form>
        </div>
    </div>
@endsection
