@extends('admin.layouts.master')
@section('title', 'Йўналишлар жадвали')
@section('content')

    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12">
                <div class="card">
                    <div class="card-header">
                        <h5>СТУ Сервисда йўналишлар жадвали</h5>
                        <span>Ушбу жадвалдан сиз таълим муассасидаги йўналишларни кўришингиз, таҳрирлаш ишларини, шунингдек йўналишни базадан ўчириб ташлашингиз мумкин.</span>
                    </div>
                    <div class="card-body">
                        <div class="table-responsive product-table">
                            <table class="display" id="basic-1">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th class="col-sm-3">йўналиш номи</th>
                                    <th class="col-sm-3">Ўқув режаси</th>
                                    <th class="col-sm-3">Назарий соат</th>
                                    <th class="col-sm-3">Амалий соат</th>
                                    <th>Таҳрирлаш</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($courses as $course)
                                    <tr>
                                        <td>{{$loop->index+1}}</td>
                                        <td class="col-sm-4">
                                            <a target="_blank" href="{{route('course.itemGroup', $course)}}">{{$course->course_title}}</a>
                                        </td>
                                        <td class="col-sm-2">
                                            {{$course->showStudyPlan->plan_title}}
                                        </td>
                                        <td class="col-sm-1">
                                            {{$course->nazariy_soat}}
                                        </td>
                                        <td class="col-sm-1">
                                            {{$course->amaliy_soat}}
                                        </td>
                                        <td class="col-sm-3">
                                            <a class="btn btn-primary" href="{{route('course.edit', $course)}}" style="padding: 6px 10px ;"><i class="far fa-edit"></i></a>
{{--                                            <form class="d-inline-block" method="post" action="{{route('course.destroy', $course)}}">--}}
{{--                                                @csrf--}}
{{--                                                @method('DELETE')--}}
{{--                                                <button type="submit" class="btn btn-danger show_confirm" data-toggle="tooltip" title='Delete' style="padding: 6px 10px ;"><i class="far fa-trash-alt"></i></button>--}}
{{--                                            </form>--}}

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
@push('script')
    <script type="text/javascript">

        $('.show_confirm').click(function(event) {
            var form =  $(this).closest("form");
            var name = $(this).data("name");
            event.preventDefault();
            swal({
                title: `Ушбу йўналишни ўчирмоқчимисиз?`,
                // text: "If you delete this, it will be gone forever.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        form.submit();
                    }
                });
        });

    </script>
@endpush
