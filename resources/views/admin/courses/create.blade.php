@extends('admin.layouts.master')
@section('title', 'Йўналиш яратиш')
@section('content')
    <div class="card">

        <div class="card-body">
            <form class="" action="{{route('course.store')}}" method="post" enctype="multipart/form-data">
                @csrf
                @method('POST')
                <div class="mb-3">
                    <label for="course_title">Йўналиш номи</label>
                    <input value="{{old('course_title')}}" name="course_title" class="form-control @error('course_title') is-invalid @enderror"
                           type="text" placeholder="Йўналиш номи">
                    @error('course_title')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                </div>
                <div class="mb-3">
                    <div class="col-form-label">Ўқув режани танланг</div>
                    <select name="stude_plan_id" class="js-example-basic-single col-sm-12 @error('stude_plan_id') is-invalid @enderror">
                        <option disabled selected>Ўқув режани танланг</option>
                            @foreach($study_plans as $study_plan)
                                <option value="{{$study_plan->id}}">{{$study_plan->plan_title}}</option>
                            @endforeach
                    </select>
                    @error('stude_plan_id')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="nazariy_soat">Назарий соат</label>
                    <input value="{{old('nazariy_soat')}}" name="nazariy_soat" class="form-control @error('nazariy_soat') is-invalid @enderror"
                           type="number" placeholder="Назарий соат">
                    @error('nazariy_soat')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                </div>
                <div class="mb-3">
                    <label for="amaliy_soat">Амалий соат</label>
                    <input value="{{old('amaliy_soat')}}" name="amaliy_soat" class="form-control @error('amaliy_soat') is-invalid @enderror"
                           type="number" placeholder="Амалий соат">
                    @error('amaliy_soat')
                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                    @enderror
                </div>

                <div class="mb-3">
                    <button class="btn btn-primary" type="submit">Йўналиш яратиш</button>
                </div>
            </form>
        </div>
    </div>
@endsection
