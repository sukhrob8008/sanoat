<?php

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\Auth\LoginController::class, 'showLoginForm']);

Route::middleware(['middleware'=>'back'])->group(function (){
    Auth::routes([
//        'register'=>false
    ]);

});

Route::group(['prefix'=>'dashboard', 'middleware'=>['auth', 'admin', 'back']], function (){
    Route::get('/', [\App\Http\Controllers\HomeController::class, 'adminpanel'])->name('adminpanel');
    Route::resource('/direction', \App\Http\Controllers\Admin\YonalishController::class);
    Route::resource('/course', \App\Http\Controllers\Admin\CourseController::class);
    Route::resource('/group', \App\Http\Controllers\Admin\GroupController::class);
    Route::resource('/student', \App\Http\Controllers\Admin\StudentController::class);
    Route::get('/active/{student}', [\App\Http\Controllers\Admin\StudentController::class, 'active'])->name('student.active');
    Route::get('/active/group/student/{student}', [\App\Http\Controllers\Admin\StudentController::class, 'activeGroupStudent'])->name('activeGroupStudent');
    Route::get('/noactive/{student}', [\App\Http\Controllers\Admin\StudentController::class, 'noactive'])->name('student.noactive');
    Route::get('/noactive/group/student/{student}', [\App\Http\Controllers\Admin\StudentController::class, 'noactiveGroupStudent'])->name('noactiveGroupStudent');
    Route::get('/certificate/{student}', [\App\Http\Controllers\Admin\StudentController::class, 'certi'])->name('student.certi');
    Route::get('/plastik/{student}', [\App\Http\Controllers\Admin\StudentController::class, 'plastik'])->name('student.plastik');
    Route::get('/enable/{group}', [\App\Http\Controllers\Admin\GroupController::class, 'active'])->name('group.active');
    Route::get('/enable_org/{course_id}/{group_id}', [\App\Http\Controllers\Admin\GroupController::class, 'active_org'])->name('group.active_org');
    Route::get('/disable_org/{course_id}/{group_id}', [\App\Http\Controllers\Admin\GroupController::class, 'noactive_org'])->name('group.noactive_org');
    Route::get('/disable/{group}', [\App\Http\Controllers\Admin\GroupController::class, 'noactive'])->name('group.noactive');
    Route::get('/course/groups/{course}', [\App\Http\Controllers\Admin\CourseController::class, 'course_item'])->name('course.itemGroup');
    Route::get('/course/groups/create/{course}', [\App\Http\Controllers\Admin\CourseController::class, 'createGroupCourse'])->name('createGroupCourse');
    Route::post('/course/groups/create/store', [\App\Http\Controllers\Admin\CourseController::class, 'createGroupStore'])->name('createGroupCourseStore');
    Route::get('/course/groups/create/student/{group}', [\App\Http\Controllers\Admin\CourseController::class, 'createGroupCourseStudent'])->name('createGroupCourseStudent');
    Route::post('/course/groups/student/store', [\App\Http\Controllers\Admin\CourseController::class, 'createGroupCourseStudentStore'])->name('createGroupCourseStudentStore');

    Route::get('/group/students/{group}', [\App\Http\Controllers\Admin\GroupController::class, 'group_item'])->name('group.itemStudent');
    Route::get('/group/students/edit/{student}', [\App\Http\Controllers\Admin\StudentController::class, 'editGroupStudent'])->name('editGroupStudent');
    Route::put('/group/students/edit/{student}', [\App\Http\Controllers\Admin\StudentController::class, 'updateGroupStudent'])->name('updateGroupStudent');
    Route::resource('/guvohnoma', \App\Http\Controllers\Admin\GuvohnomaController::class);
    Route::get('/edit_org/{course_id}/{group_id}', [\App\Http\Controllers\Admin\GroupController::class, 'edit_org'])->name('group.edit_org');
    Route::put('/group_org/{course_id}/{group_id}', [\App\Http\Controllers\Admin\GroupController::class, 'update_org'])->name('group.update_org');
});

Route::get('/users/{student}',[\App\Http\Controllers\Admin\StudentController::class, 'show'])->name('show.info');
