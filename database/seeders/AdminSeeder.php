<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class AdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i=1;$i<=2; $i++){
            if($i==1){
                DB::table('users')->insert([
                    'name' => config('settings.admin_firstname'),
                    'lname' => config('settings.admin_firstname'),
                    'alogin' => config('settings.admin_login'),
                    'password' => Hash::make(config('settings.admin_password')),
                    'role' => 1,
                ]);
            }else{
                DB::table('users')->insert([
                    'name' => config('settings.admin_two_firstname'),
                    'lname' => config('settings.admin_two_firstname'),
                    'alogin' => config('settings.admin_two_login'),
                    'password' => Hash::make(config('settings.admin_two_password')),
                    'role' => 1,
                ]);
            }
        }


    }
}
