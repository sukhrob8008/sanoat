<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStudentsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('students', function (Blueprint $table) {
            $table->id();
            $table->string('student_fio');
            $table->bigInteger('group_id')->unsigned();
            $table->string('malaka');
            $table->integer('guvohnoma_id');
            $table->string('razryad')->default('-')->nullable();
            $table->string('pasport_seria');
            $table->string('pasport_number');
            $table->integer('amaliy_baho');
            $table->integer('nazariy_baho');
            $table->string('profession');
            $table->string('student_img');
            $table->string('qr_name');
            $table->integer('active')->default(1);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('students');
    }
}
