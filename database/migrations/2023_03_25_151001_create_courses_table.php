<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->id();
            $table->integer('stude_plan_id')->unsigned();
            $table->string('course_title');
            $table->bigInteger('amaliy_soat')->unsigned();
            $table->bigInteger('nazariy_soat')->unsigned();
            $table->timestamps();

//            $table->foreign('stude_plan_id')->references('id')->on('yonalishes')->onDelete('cascade');

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
